package leetcode;

import org.junit.Assert;
import org.junit.Test;

public class TestTreeNode {
	@Test
	public void shouldOutputTree() {
		TreeNode root = TreeNode.parseString("[2, 1, 3]");
		String output = root.toString();
		Assert.assertEquals("2,1,3", output);
	}
}
