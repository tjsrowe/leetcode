package leetcode;

import org.junit.Assert;
import org.junit.Test;

public class TestMyLinkedList {
	/**
	 * Your MyLinkedList object will be instantiated and called as such:
	 * MyLinkedList obj = new MyLinkedList();
	 * int param_1 = obj.get(index);
	 * obj.addAtHead(val);
	 * obj.addAtTail(val);
	 * obj.addAtIndex(index,val);
	 * obj.deleteAtIndex(index);
	 */
	
	@Test
	public void shouldHaveHeadAtStart() {
		MyLinkedList obj = new MyLinkedList();
		obj.addAtHead(1);
		
		int headValue = obj.get(0);
		Assert.assertEquals(1, headValue);
	}

	@Test
	public void shouldInsertAtHead() {
		MyLinkedList obj = new MyLinkedList();
		obj.addAtHead(1);
		obj.addAtHead(2);
		
		int headValue = obj.get(0);
		Assert.assertEquals(2, headValue);
		int secondValue = obj.get(1);
		Assert.assertEquals(1, secondValue);
	}

	@Test
	public void shouldHeadTailOnly() {
		MyLinkedList obj = new MyLinkedList();
		obj.addAtTail(1);
		
		int headValue = obj.get(0);
		Assert.assertEquals(1, headValue);
	}

	@Test
	public void shouldHaveTailAfterHead() {
		MyLinkedList obj = new MyLinkedList();
		obj.addAtHead(1);
		obj.addAtTail(2);
		
		int headValue = obj.get(0);
		Assert.assertEquals(1, headValue);
		int tailValue = obj.get(1);
		Assert.assertEquals(2, tailValue);
	}
	
	@Test
	public void shouldHaveTailAfterHeadAdded() {
		MyLinkedList obj = new MyLinkedList();
		obj.addAtTail(2);
		obj.addAtHead(1);
		
		int headValue = obj.get(0);
		Assert.assertEquals(1, headValue);
		int tailValue = obj.get(1);
		Assert.assertEquals(2, tailValue);
	}
	
	@Test
	public void shouldInsertBetween() {
		MyLinkedList obj = new MyLinkedList();
		obj.addAtHead(1);
		obj.addAtTail(2);
		obj.addAtIndex(1, 3);
		
		int headValue = obj.get(0);
		Assert.assertEquals(1, headValue);
		int middleValue = obj.get(1);
		Assert.assertEquals(3, middleValue);
		int tailValue = obj.get(2);
		Assert.assertEquals(2, tailValue);
	}
	
	@Test
	public void shouldHaveNewValueAtTail() {
		MyLinkedList obj = new MyLinkedList();
		obj.addAtHead(1);
		obj.addAtTail(2);
		obj.addAtIndex(2, 3);
		int tailValue = obj.get(2);
		Assert.assertEquals(3, tailValue);
	}

	@Test
	public void shouldAddAtStartIndex() {
		MyLinkedList obj = new MyLinkedList();
		obj.addAtHead(1);
		obj.addAtTail(2);
		obj.addAtIndex(0, 3);
		int headValue = obj.get(0);
		Assert.assertEquals(3, headValue);
	}
	
	@Test
	public void shouldInsertAtStartIndex() {
		MyLinkedList obj = new MyLinkedList();
		obj.addAtHead(1);
		obj.addAtTail(2);
		obj.addAtIndex(1, 3);
		obj.addAtIndex(0, 4);
		
		int headValue = obj.get(0);
		Assert.assertEquals(4, headValue);
		int middleValue1 = obj.get(1);
		Assert.assertEquals(1, middleValue1);
		int middleValue2 = obj.get(2);
		Assert.assertEquals(3, middleValue2);
		int tailValue = obj.get(3);
		Assert.assertEquals(2, tailValue);
	}
	
	@Test
	public void shouldAddMiddleAfterNewTail() {
		MyLinkedList obj = new MyLinkedList();
		obj.addAtHead(2);
		System.out.println(obj);
		obj.addAtTail(4);
		System.out.println(obj);
		obj.addAtIndex(1, 3);
		System.out.println(obj);
		obj.addAtIndex(0, 0);
		System.out.println(obj);
		obj.addAtTail(5);
		System.out.println(obj);
		obj.addAtIndex(1, 1);
		System.out.println(obj);
		
		int headValue = obj.get(0);
		Assert.assertEquals(0, headValue);
		int middleValue1 = obj.get(1);
		Assert.assertEquals(1, middleValue1);
		int middleValue2 = obj.get(3);
		Assert.assertEquals(3, middleValue2);
		int tailValue = obj.get(5);
		Assert.assertEquals(5, tailValue);
	}

	@Test
	public void shouldRemoveMiddle() {
		MyLinkedList obj = new MyLinkedList();
		obj.addAtHead(0);
		obj.addAtTail(2);
		obj.addAtIndex(1, 1);
		
		Assert.assertEquals(1, obj.get(1));
		obj.deleteAtIndex(1);
		Assert.assertEquals(0, obj.get(0));
		Assert.assertEquals(2, obj.get(1));
	}
	
	@Test
	public void shouldRemoveFirst() {
		MyLinkedList obj = new MyLinkedList();
		obj.addAtHead(0);
		obj.addAtTail(2);
		obj.addAtIndex(1, 1);
		
		obj.deleteAtIndex(0);
		Assert.assertEquals(1, obj.get(0));
		Assert.assertEquals(2, obj.get(1));
	}
	
	@Test
	public void shouldRemoveLast() {
		MyLinkedList obj = new MyLinkedList();
		obj.addAtHead(0);
		obj.addAtTail(2);
		obj.addAtIndex(1, 1);
		
		obj.deleteAtIndex(2);
		Assert.assertEquals(0, obj.get(0));
		Assert.assertEquals(1, obj.get(1));
	}
	
	@Test
	public void shouldRemoveOnlyElement() {
		MyLinkedList obj = new MyLinkedList();
		obj.addAtHead(0);
		obj.deleteAtIndex(0);
		Assert.assertEquals(-1, obj.get(0));
	}
	
	@Test
	public void shouldNotDeletePastLastIndex() {
		MyLinkedList obj = new MyLinkedList();
		obj.addAtHead(0);
		obj.deleteAtIndex(1);
		Assert.assertEquals(0, obj.get(0));
	}
	
	@Test
	public void exampleTestOne() {
		MyLinkedList obj = new MyLinkedList();
		obj.addAtHead(2);
		obj.deleteAtIndex(1);
		obj.addAtHead(2);
		obj.addAtHead(7);
		obj.addAtHead(3);
		obj.addAtHead(2);
		obj.addAtHead(5);
		obj.addAtTail(5);
		// get
		int expected = 2;
		int actual = obj.get(5);
		Assert.assertEquals(expected, actual);
		obj.deleteAtIndex(6);
		obj.deleteAtIndex(4);
		
//		Assert.assertEquals(-1, obj.get(0));
	}
	
	@Test
	public void shouldNotAddAtIndexPastExistingElements() {
		MyLinkedList obj = new MyLinkedList();
		obj.addAtIndex(1, 0);
		int result = obj.get(0);
		Assert.assertEquals(-1, result);
	}
}
