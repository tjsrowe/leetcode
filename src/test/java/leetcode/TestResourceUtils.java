package leetcode;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import leetcode.utils.ResourceUtils;

public class TestResourceUtils {
	@Test
	public void testThreeSumLargeDataset() throws IOException {
		int [] data = ResourceUtils.loadIntArrayResource("../../threesum/threesum-largedataset.txt");
		Assert.assertEquals(3000, data.length);
	}
}
