package leetcode;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class RankTeamsByVotes {
	@Test
	public void exampleOne() {
		final String[] votes = { "ABC", "ACB", "ABC", "ACB", "ACB" };
		final String expectedOutput = "ACB";
		final String output = this.rankTeams(votes);
		Assert.assertEquals(expectedOutput, output);
	}

	@Test
	public void exampleTwo() {
		final String[] votes = { "WXYZ", "XYZW" };
		final String expectedOutput = "XWYZ";
		final String output = this.rankTeams(votes);
		Assert.assertEquals(expectedOutput, output);
	}

	@Test
	public void exampleThree() {
		final String[] votes = { "ZMNAGUEDSJYLBOPHRQICWFXTVK" };
		final String expectedOutput = "ZMNAGUEDSJYLBOPHRQICWFXTVK";
		final String output = this.rankTeams(votes);
		Assert.assertEquals(expectedOutput, output);
	}

	@Test
	public void exampleFour() {
		final String[] votes = { "BCA", "CAB", "CBA", "ABC", "ACB", "BAC" };
		final String expectedOutput = "ABC";
		final String output = this.rankTeams(votes);
		Assert.assertEquals(expectedOutput, output);
	}

	@Test
	public void exampleFive() {
		final String[] votes = { "M", "M", "M", "M" };
		final String expectedOutput = "M";
		final String output = this.rankTeams(votes);
		Assert.assertEquals(expectedOutput, output);
	}

	@Test
	public void shouldReturnEmptyString() {
		final String[] votes = {  };
		final String expectedOutput = "";
		final String output = this.rankTeams(votes);
		Assert.assertEquals(expectedOutput, output);
	}
	
	public void addScoreToArray(HashMap<Character, int[]> scores, String votes) {
		char[] varr = votes.toCharArray();
		for (int i = 0; i < varr.length; i++) {
			char team = varr[i];
			int[] teamScores = null;
			if (!scores.containsKey(team)) {
				teamScores = new int[votes.length()];
				scores.put(team, teamScores);
			} else {
				teamScores = scores.get(team);
			}
			teamScores[i] = teamScores[i] + 1;
		}
	}

	public String rankTeams(String[] votes) {
		HashMap<Character, int[]> scores = new HashMap<Character, int[]>();
		for (String vote : votes) {
			addScoreToArray(scores, vote);
		}
		List<Character> teams = sortTeams(scores);
		final String output = buildOutput(teams);
		return output;
	}

	private String buildOutput(List<Character> teams) {
		StringBuilder sbOutput = new StringBuilder();
		for (int i = 0; i < teams.size(); i++) {
			Character currentTeam = teams.get(i);
			sbOutput.append(currentTeam);
		}
		final String output = sbOutput.toString();
		return output;
	}

	private List<Character> sortTeams(HashMap<Character, int[]> scores) {
		List<Character> teams = new ArrayList<Character>(scores.keySet());
		teams.sort(new Comparator<Character>() {
			public int compare(Character c1, Character c2) {
				int[] scores1 = scores.get(c1);
				int[] scores2 = scores.get(c2);
				for (int i = 0; i < scores1.length; i++) {
					if (scores2[i] != scores1[i]) {
						return scores2[i] - scores1[i];
					}
				}
				return 0;
			};
		});
		return teams;
	}
}
