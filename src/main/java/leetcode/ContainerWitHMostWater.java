package leetcode;

import org.junit.Assert;
import org.junit.Test;

public class ContainerWitHMostWater {
	@Test
	public void exampleOne() {
		int [] height = {1, 8, 6, 2, 5, 4, 8, 3, 7};
		int expectedOutput = 49;
		
		int output = maxArea(height);
		Assert.assertEquals(expectedOutput,  output);
	}
	
	@Test
	public void exampleTwo() {
		int [] height = {1, 1};
		int expectedOutput = 1;
		
		int output = maxArea(height);
		Assert.assertEquals(expectedOutput,  output);
	}
	
	@Test
	public void exampleThree() {
		int [] height = {4, 3, 2, 1, 4};
		int expectedOutput = 16;
		
		int output = maxArea(height);
		Assert.assertEquals(expectedOutput,  output);
	}
	
	@Test
	public void exampleFour() {
		int [] height = {1, 2, 1};
		int expectedOutput = 2;
		
		int output = maxArea(height);
		Assert.assertEquals(expectedOutput,  output);
	}
	
	int min(int a, int b) {
		if (b < a) {
			return b; 
		}
		return a;
	}
	
    public int maxArea(int[] height) {
        int start = 0;
        int end = height.length-1;
        int highestStart = -1;
        int highestEnd = -1;
        int maxVolume = -1;
        
        while (start < end) {
        	int width = end - start;
        	int min = min(height[start], height[end]);
        	int volume = width * min;
        	if (volume > maxVolume) {
        		highestStart = start;
        		highestEnd = end;
        		maxVolume = volume;
        	}
        	
        	if (height[end] < height[start]) {
        		end--;
        	} else {
        		start++;
        	}
        }
        return maxVolume;
    }
}
