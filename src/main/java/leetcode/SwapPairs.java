package leetcode;

import org.junit.Assert;
import org.junit.Test;

public class SwapPairs {
	@Test
	public void exampleOne() {
		ListNode input = ListNode.toList(new int[] { 1, 2, 3, 4 });
		ListNode expected = ListNode.toList(new int[] { 2, 1, 4, 3 });
		ListNode actual = this.swapPairs(input);
		Assert.assertEquals(expected.toString(), actual.toString());
	}

	@Test
	public void exampleThree() {
		ListNode input = ListNode.toList(new int[] { 1 });
		ListNode expected = ListNode.toList(new int[] { 1 });
		ListNode actual = this.swapPairs(input);
		Assert.assertEquals(expected.toString(), actual.toString());
	}

	@Test
	public void exampleTwo() {
		ListNode input = ListNode.toList(new int[] {});
		ListNode expected = ListNode.toList(new int[] {});
		ListNode actual = this.swapPairs(input);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void exampleFour() {
		ListNode input = ListNode.toList(new int[] { 1, 2, 3, 4, 5 });
		ListNode expected = ListNode.toList(new int[] { 2, 1, 4, 3, 5 });
		ListNode actual = this.swapPairs(input);
		Assert.assertEquals(expected.toString(), actual.toString());
	}
	public ListNode swapPairs(ListNode head) {
		if (head == null) {
			return null;
		}
		if (head.next != null) {
			ListNode nextNode = head.next.next;
			ListNode newHead = head.next;
			newHead.next = head;
			head.next = swapPairs(nextNode);
			return newHead;
		} else {
			return head;
		}
	}
}
