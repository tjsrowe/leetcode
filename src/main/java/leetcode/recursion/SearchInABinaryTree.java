package leetcode.recursion;

import org.junit.Assert;
import org.junit.Test;

import leetcode.TreeNode;

public class SearchInABinaryTree {
	@Test
	public void exampleOne() {
		TreeNode root = TreeNode.parseString("[4,2,7,1,3]");
		int val = 2;
		TreeNode returnedNode = this.searchBST(root, val);
//		TreeNode expectedOutputTree = TreeNode.parseString("[2,1,3]");
		
		String strOutput = returnedNode.toString();
		Assert.assertEquals("2,1,3", strOutput);
	}
	
	public TreeNode searchBST(TreeNode root, int val) {
		if (root == null) {
			return null;
		}
		if (root.val == val) {
			return root;
		}
		if (val < root.val) {
			return searchBST(root.left, val);
		} else {
			return searchBST(root.right, val);
		}
    }
}
