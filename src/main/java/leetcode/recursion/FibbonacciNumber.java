package leetcode.recursion;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

public class FibbonacciNumber {
	@Test
	public void exampleOne() {
		final int n = 2;
		final int output = 1;
		Assert.assertEquals(output,  this.fib(n));
	}

	@Test
	public void exampleSix() {
		final int n = 0;
		final int output = 0;
		Assert.assertEquals(output,  this.fib(n));
	}
	
	@Test
	public void exampleSeven() {
		final int n = 1;
		final int output = 1;
		Assert.assertEquals(output,  this.fib(n));
	}
	
	@Test
	public void exampleTwo() {
		final int n = 3;
		final int output = 2;
		Assert.assertEquals(output,  this.fib(n));
	}
	
	@Test
	public void exampleThree() {
		final int n = 4;
		final int output = 3;
		Assert.assertEquals(output,  this.fib(n));
	}

	@Test
	public void exampleFour() {
		final int n = 14;
		final int output = 377;
		Assert.assertEquals(output,  this.fib(n));
	}
	
	@Test
	public void exampleFive() {
		final int n = 46;
		final int output = 1836311903;
		Assert.assertEquals(output,  this.fib(n));
	}
	
	int [] values = null;
	
    public int fib(int n) {
    	if (values == null) {
    		values = new int[Math.max(3, n+1)];
    		values[0] = 0;
    		values[1] = 1;
    		values[2] = 1;
    		for (int i = 3; i < values.length;i++) {
    			values[i] = -1;
    		}
    	}
    	if (n > 2 && values[n] < 0) {
        	values[n] = fib(n - 1) + fib(n - 2);
    	}
        return values[n];
    }
	
	Map<Integer, Integer> cachedValues = new HashMap<Integer, Integer>();
	
    public int simpleFib(int n) {
    	if (n == 0) {
    		return 0;
    	}
    	if (n <= 2) {
    		return 1;
    	}
    	
    	if (cachedValues.containsKey(n)) {
    		return cachedValues.get(n);
    	}
    	
    	int m1 = fib(n - 1);
    	int m2 = fib(n - 2);
    	
    	
    	int total = m1 + m2;
    	cachedValues.put(n,  total);
    	return total;
    }
}
