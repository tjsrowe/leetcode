package leetcode.recursion;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

public class ClimbingStairs {
	@Test
	public void exampleOne() {
		final int n = 2;
		final int expectedOutput = 2;
		final int output = this.climbStairs(n);
		Assert.assertEquals(expectedOutput, output);
	}
	
	@Test
	public void exampleTwo() {
		final int n = 3;
		final int expectedOutput = 3;
		final int output = this.climbStairs(n);
		Assert.assertEquals(expectedOutput, output);
	}
	
	@Test
	public void exampleThree() {
		final int n = 5;
		final int expectedOutput = 8;
		final int output = this.climbStairs(n);
		Assert.assertEquals(expectedOutput, output);
	}
	
	Map<Integer, Integer> steps = new HashMap<Integer, Integer>() {{ put(0, 0); put(1, 1); put(2, 2);}};
	
    public int climbStairs(int n) {
    	if (steps.containsKey(n)) {
    		return steps.get(n);
    	}
    	
    	int result = climbStairs(n - 1) + climbStairs(n - 2);
    	steps.put(n, result);
    	return result;
    }
}
