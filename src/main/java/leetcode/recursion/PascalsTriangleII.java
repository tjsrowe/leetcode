package leetcode.recursion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

public class PascalsTriangleII {
	@Test
	public void exampleOne() {
		final int rowIndex = 3;
		List<Integer> expectedOutput = Arrays.asList(1, 3, 3, 1);
		List<Integer> output = this.getRow(rowIndex);
		Assert.assertEquals(expectedOutput, output);
	}
	
	@Test
	public void exampleFour() {
		final int rowIndex = 11;
		List<Integer> expectedOutput = Arrays.asList(1, 11, 55, 165, 330, 462, 462, 330, 165, 55, 11, 1);
		List<Integer> output = this.getRow(rowIndex);
		Assert.assertEquals(expectedOutput, output);
	}
	
	@Test
	public void exampleTwo() {
		final int rowIndex = 0;
		List<Integer> expectedOutput = Arrays.asList(1);
		List<Integer> output = this.getRow(rowIndex);
		Assert.assertEquals(expectedOutput, output);
	}
	
	@Test
	public void exampleThree() {
		final int rowIndex = 1;
		List<Integer> expectedOutput = Arrays.asList(1, 1);
		List<Integer> output = this.getRow(rowIndex);
		Assert.assertEquals(expectedOutput, output);
	}
	
    public List<Integer> getRow(int rowIndex) {
    	if (rowIndex == 1) {
    		return Arrays.asList(1, 1);
    	} else if (rowIndex == 0) {
    		return Arrays.asList(1);
    	}
    	
    	List<Integer> previousRow = getRow(rowIndex - 1);
    	List<Integer> currentRow = new ArrayList<Integer>();
    	currentRow.add(1);
    	int midPoint = rowIndex % 2 == 0 ? (rowIndex / 2) : (rowIndex - 1) / 2;
    	Map<Integer, Integer> precalculatedValues = new HashMap<Integer, Integer>();
    	for (int i = 1;i < rowIndex;i++) {
    		int oppositeIndex = rowIndex - i;
    		if (i != oppositeIndex) {
    			int previousValue = previousRow.get(i - 1) + previousRow.get(i);
    			precalculatedValues.put(oppositeIndex, previousValue);
    			currentRow.add(previousValue);
    		} else if (i > midPoint) {
    			int val = precalculatedValues.get(oppositeIndex);
    			currentRow.add(val);
    		} else {
    			int previousValue = previousRow.get(i - 1) + previousRow.get(i);
    			currentRow.add(previousValue);
    		}
    	}
    	currentRow.add(1);
    	return currentRow;
    }
}
