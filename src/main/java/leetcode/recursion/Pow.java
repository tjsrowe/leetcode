package leetcode.recursion;

import org.junit.Assert;
import org.junit.Test;

public class Pow {
	@Test
	public void exampleOne() {
		final double x = 2.00000;
		final int n = 10;
		final double expectedOutput = 1024.00000;
		final double output = this.myPow(x, n);
		Assert.assertEquals(expectedOutput, output, 0.00005);
	}
	
	@Test
	public void exampleTwo() {
		final double x = 2.10000;
		final int n = 3;
		final double expectedOutput = 9.26100;
		final double output = this.myPow(x, n);
		Assert.assertEquals(expectedOutput, output, 0.00005);
	}
	
	@Test
	public void exampleThree() {
		final double x = 2.00000;
		final int n = -2;
		final double expectedOutput = 0.25000;
		final double output = this.myPow(x, n);
		Assert.assertEquals(expectedOutput, output, 0.00005);
	}
	
	@Test
	public void exampleFour() {
		final double x = 34.00515;
		final int n = -3;
		final double expectedOutput = 3e-05;
		final double output = this.myPow(x, n);
		// 34.00515 ^ -3 = 1/34.00515^3
		Assert.assertEquals(expectedOutput, output, 0.0000005);
	}
	
	@Test
	public void exampleFive() {
		final double x = 0.44528;
		final int n = 0;
		final double expectedOutput = 1.00000;
		final double output = this.myPow(x, n);
		// 34.00515 ^ -3 = 1/34.00515^3
		Assert.assertEquals(expectedOutput, output, 0.0000005);
	}
	
	@Test
	public void exampleSix() {
		final double x = 0.44528;
		final int n = 655;
		final double expectedOutput = 0.000005;
		final double output = this.myPow(x, n);
		Assert.assertEquals(expectedOutput, output, 0.0000005);
	}
	
	@Test
	public void exampleSeven() {
		final double x = -2.0;
		final int n = 2;
		final double expectedOutput = 4.00000;
		final double output = this.myPow(x, n);
		Assert.assertEquals(expectedOutput, output, 0.0000005);
	}

	@Test
	public void exampleEleven() {
		final double x = 2.0000;
		final int n = 10;
		final double expectedOutput = 1024.00000;
		final double output = this.myPow(x, n);
		Assert.assertEquals(expectedOutput, output, 0.0000005);
	}
	
	@Test
	public void exampleNine() {
		final double x = 1.0;
		final int n = 2147483647;
		final double expectedOutput = 1.00000;
		final double output = this.myPow(x, n);
		Assert.assertEquals(expectedOutput, output, 0.0000005);
	}
	
	@Test
	public void exampleTwelve() {
		final double x = 1.0;
		final int n = -2147483648;
		final double expectedOutput = 1.00000;
		final double output = this.myPow(x, n);
		Assert.assertEquals(expectedOutput, output, 0.0000005);
	}
	
	@Test
	public void exampleThirteen() {
		final double x = 4.0;
		final int n = -2147483648;
		final double expectedOutput = 1.00000;
		final double output = this.myPow(x, n);
		Assert.assertEquals(expectedOutput, output, 0.0000005);
	}
	
	@Test
	public void exampleTen() {
		final double x = 1.00001;
		final int n = 2147483647;
		final double expectedOutput = 1.00001;
		final double output = this.myPow(x, n);
		Assert.assertEquals(expectedOutput, output, 0.0000005);
	}
	
	@Test
	public void exampleEight() {
		final double x = -2.0;
		final int n = 3;
		final double expectedOutput = -8.00000;
		final double output = this.myPow(x, n);
		Assert.assertEquals(expectedOutput, output, 0.0000005);
	}
	
	public double helperMyPow(int totalSteps, int stepsRemaining, double orig, double current) {
		if (stepsRemaining > 2 && stepsRemaining % 2 == 0) {
			double val = current * orig;
			double hpow = helperMyPow(totalSteps, (stepsRemaining / 2) - 1, orig, val);
			return hpow * hpow;
		}
		else if (stepsRemaining == 0) {
			return current;
		} else if (current > 0 && current < 0.000005) {
			return current;
		} else if (current < 0 && (0 + current) < 0.000005 && (0 - current) < 0.000005) {
			return current;
		} else if (orig == current && totalSteps != stepsRemaining) {
			return current;
//		} else if (stepsRemaining % 2 == 0) {
//			double hpow = helperMyPow(totalSteps/2, stepsRemaining / 2, orig, current);
//			return hpow * hpow;
		} else {
			double val = current * orig;
			return helperMyPow(totalSteps, stepsRemaining - 1, orig, val);
		}
	}
	
    public double myPow2(double x, int n) {
    	if (n == Integer.MIN_VALUE) {
        	double pow = myPow(x, Integer.MAX_VALUE);
        	pow *= x;
        	return 1 / pow;
    	} else if (n < 0) {
        	double pow = myPow(x, n * -1);
        	return 1 / pow;
        } else if ( n == 1) {
        	return x;
        } else if (n == 0) {
        	return 1;
        } else {
        	return helperMyPow(n, n, x, x);
        }
    }
    
    public double myPow(double x, int n) {
        if (n == 0) {
            return 1;
        }
        double d = myPow(x * x, n / 2);
        if (n % 2 == 1) {
            d *= x;
        } else if (n % 2 == -1) {
            d /= x;
        }
        
        return d;
    }
}
