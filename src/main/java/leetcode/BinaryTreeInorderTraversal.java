package leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class BinaryTreeInorderTraversal {
	@Test
	public void exampleOne() {
		TreeNode data = TreeNode.parseString("[1,null,2,3]");
		List<Integer> output = this.inorderTraversal(data);

		Assert.assertEquals(Arrays.asList(1, 3, 2), output);
	}

	@Test
	public void exampleTwo() {
		TreeNode data = TreeNode.parseString("[]");
		List<Integer> output = this.inorderTraversal(data);

		Assert.assertEquals(Arrays.asList(), output);
	}

	@Test
	public void exampleThree() {
		TreeNode data = TreeNode.parseString("[1]");
		List<Integer> output = this.inorderTraversal(data);

		Assert.assertEquals(Arrays.asList(1), output);
	}

	@Test
	public void exampleFour() {
		TreeNode data = TreeNode.parseString("[1,2]");
		List<Integer> output = this.inorderTraversal(data);

		Assert.assertEquals(Arrays.asList(2, 1), output);
	}

	@Test
	public void exampleFive() {
		TreeNode data = TreeNode.parseString("[1,null,2]");
		List<Integer> output = this.inorderTraversal(data);

		Assert.assertEquals(Arrays.asList(1, 2), output);
	}

	public List<Integer> inorderTraversal(TreeNode root) {
		List<Integer> output = new ArrayList<Integer>();

		if (root != null) {
			if (root.left != null) {
				List<Integer> left = inorderTraversal(root.left);
				output.addAll(left);
			}
			output.add(root.val);
			if (root.right != null) {
				List<Integer> right = inorderTraversal(root.right);
				output.addAll(right);
			}
		}

		return output;
	}

}
