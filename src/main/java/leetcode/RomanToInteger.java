package leetcode;

import org.junit.Assert;
import org.junit.Test;

public class RomanToInteger {
	@Test
	public void exampleOne() {
		final String s = "III";
		final int output = 3;
		final int result = this.romanToInt(s);
		Assert.assertEquals(output, result);
	}
	
	@Test
	public void exampleTwo() {
		final String s = "IV";
		final int output = 4;
		final int result = this.romanToInt(s);
		Assert.assertEquals(output, result);
	}
	
	@Test
	public void exampleThree() {
		final String s = "IX";
		final int output = 9;
		final int result = this.romanToInt(s);
		Assert.assertEquals(output, result);
	}
	
	@Test
	public void exampleFour() {
		final String s = "LVIII";
		final int output = 58;
		final int result = this.romanToInt(s);
		Assert.assertEquals(output, result);
	}
	
	@Test
	public void exampleFive() {
		final String s = "MCMXCIV";
		final int output = 1994;
		final int result = this.romanToInt(s);
		Assert.assertEquals(output, result);
	}
	
	private static final int highest(int modif, int currentHighest) {
		if (modif > currentHighest) {
			return modif;
		}
		return currentHighest;
	}
	
    public int romanToInt(String s) {
        char [] str = s.toCharArray();
        int sum = 0;
        int add = 0;
        int highestUsed = 0;
        for (int i = str.length-1;i >= 0;i--) {
        	int ch = str[i];
        	int modif = 1;
        	switch (ch) {
	        	case 'M':
	        		modif *= 10;
	        	case 'D':
	        	case 'C':
	        		modif *= 10;
	        	case 'X':
	        	case 'L':
	        		modif *= 10;
        	}
        	switch (ch) {
	        	case 'V':
	        	case 'L':
	        	case 'D':
	        		modif *= 5;
        	}
        	highestUsed = highest(modif, highestUsed);
        	
        	if (modif < highestUsed) {
        		add -= modif;
        	} else {
        		add += modif;
        	}
        }
        sum += add;
        return sum;
    }
}
