package leetcode.topint;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

public class PlusOne {
	@Test
	public void exampleOne() {
		int[] input = new int[] { 1, 2, 3 };
		int[] output = new int[] { 1, 2, 4 };
		int[] result = this.plusOne(input);
		Assert.assertTrue(Arrays.equals(output, result));
	}

	@Test
	public void exampleTwo() {
		int[] input = new int[] { 4, 3, 2, 1 };
		int[] output = new int[] { 4, 3, 2, 2 };
		int[] result = this.plusOne(input);
		Assert.assertTrue(Arrays.equals(output, result));
	}

	@Test
	public void exampleThree() {
		int[] input = new int[] { 0 };
		int[] output = new int[] { 1 };
		int[] result = this.plusOne(input);
		Assert.assertTrue(Arrays.equals(output, result));
	}

	@Test
	public void exampleFour() {
		int[] input = new int[] { 9 };
		int[] output = new int[] { 1, 0 };
		int[] result = this.plusOne(input);
		Assert.assertTrue(Arrays.equals(output, result));
	}

	int[] incrementIndex(int index, int[] digits) {
		if (digits[index] == 9) {
			digits[index] = 0;
			if (index == 0) {
				int[] newArray = new int[digits.length + 1];
				int newIndex = 0;
				int oldIndex = 0;
				newArray[newIndex++] = 1;
				while (newIndex < newArray.length) {
					newArray[newIndex++] = digits[oldIndex++];
				}
				return newArray;
			} else {
				return incrementIndex(index - 1, digits);
			}
		} else {
			digits[index] = digits[index] + 1;
			return digits;
		}
	}

	public int[] plusOne(int[] digits) {
		int[] output = incrementIndex(digits.length - 1, digits);
		return output;
	}
}
