package leetcode.topint;

import org.junit.Assert;
import org.junit.Test;

import leetcode.TreeNode;

public class SymmetricTree {
	@Test
	public void exampleOne() {
		TreeNode data = TreeNode.parseString("[1,2,2,3,4,4,3]");
		boolean result = this.isSymmetric(data);
		Assert.assertTrue(result);
	}

	@Test
	public void exampleTwo() {
		TreeNode data = TreeNode.parseString("[1,2,2,null,3,null,3]");
		boolean result = this.isSymmetric(data);
		Assert.assertFalse(result);
	}

	@Test
	public void exampleThree() {
		TreeNode data = TreeNode.parseString("[2,3,3,4,5,null,4]");
		boolean result = this.isSymmetric(data);
		Assert.assertFalse(result);
	}

	@Test
	public void exampleFour() {
		TreeNode data = TreeNode.parseString("[2,3,3,4,null,5,4]");
		boolean result = this.isSymmetric(data);
		Assert.assertFalse(result);
	}

	public static boolean matchNull(TreeNode left, TreeNode right) {
		if (left == null && right != null) {
			return false;
		}
		if (right == null && left != null) {
			return false;
		}
		return true;
	}

	public boolean compareMirror(TreeNode left, TreeNode right) {
		if (left == null && right == null) {
			return true;
		}
		if (!matchNull(left.left, right.right) || !matchNull(left.right, right.left)) {
			return false;
		}
		if (left.val != right.val) {
			return false;
		}
		boolean outerEqual = compareMirror(left.left, right.right);
		if (!outerEqual) {
			return false;
		}
		boolean innerEqual = compareMirror(left.right, right.left);
		if (!innerEqual) {
			return false;
		}
		return true;
	}

	public boolean isSymmetric(TreeNode root) {
		if (root.left == null && root.right == null) {
			return true;
		}
		if ((root.left == null && root.right != null) || (root.left != null && root.right == null)) {
			return false;
		}
		return compareMirror(root.left, root.right);
	}
}
