package leetcode.topint;

import leetcode.TreeNode;

public class MaximumDepthOfBinaryTree {
	class Solution {
		public int maxDepth(TreeNode root) {
			int maxDepth = 0;
			if (root == null) {
				return 0;
			}
			if (root.left != null) {
				int nodeDepth = maxDepth(root.left);
				if (nodeDepth > maxDepth) {
					maxDepth = nodeDepth;
				}
			}
			if (root.right != null) {
				int nodeDepth = maxDepth(root.right);
				if (nodeDepth > maxDepth) {
					maxDepth = nodeDepth;
				}
			}
			return maxDepth + 1;
		}
	}
}
