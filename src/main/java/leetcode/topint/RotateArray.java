package leetcode.topint;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

public class RotateArray {
	@Test
	public void exampleOne() {
		int[] nums = new int[] { 1, 2, 3, 4, 5, 6, 7 };
		int[] expected = new int[] { 5, 6, 7, 1, 2, 3, 4 };
		rotate(nums, 3);
		Assert.assertTrue(Arrays.equals(expected, nums));
	}

	@Test
	public void exampleTwo() {
		int[] nums = new int[] { -1, -100, 3, 99 };
		int[] expected = new int[] { 3, 99, -1, -100 };
		rotate(nums, 2);
		Assert.assertTrue(Arrays.equals(expected, nums));
	}

	public void reverse(int[] nums, int start, int end) {
		while (start < end) {
			int tmp = nums[end];
			nums[end] = nums[start];
			nums[start] = tmp;
			start++;
			end--;
		}
	}

	public void rotate(int[] nums, int k) {
		k %= nums.length;
		reverse(nums, 0, nums.length - 1);
		reverse(nums, 0, k - 1);
		reverse(nums, k, nums.length - 1);
	}
}
