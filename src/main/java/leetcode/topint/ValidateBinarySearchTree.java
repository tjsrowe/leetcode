package leetcode.topint;

import org.junit.Assert;
import org.junit.Test;

import leetcode.TreeNode;

public class ValidateBinarySearchTree {
	@Test
	public void exampleTwo() {
		TreeNode data = TreeNode.parseString("[5,1,4,null,null,3,6]");
		boolean isValid = this.isValidBST(data);
		Assert.assertFalse(isValid);
	}

	@Test
	public void exampleThree() {
		TreeNode data = TreeNode.parseString("[3,1,5,0,2,4,6,null,null,null,3]");
		boolean isValid = this.isValidBST(data);
		Assert.assertFalse(isValid);
	}

	boolean hasMaxNode(TreeNode root, int max) {
		boolean isValid = true;
		if (root != null) {
			if (root.val >= max) {
				isValid = false;
			} else {
				boolean leftOk = hasMaxNode(root.left, max);
				if (!leftOk) {
					isValid = false;
				} else {
					boolean rightOk = hasMaxNode(root.right, max);
					if (!rightOk) {
						isValid = false;
					}
				}
			}
		}
		return isValid;
	}

	boolean hasMinNode(TreeNode root, int min) {
		boolean isValid = true;
		if (root != null) {
			if (root.val <= min) {
				isValid = false;
			} else {
				boolean leftOk = hasMinNode(root.left, min);
				if (!leftOk) {
					isValid = false;
				} else {
					boolean rightOk = hasMinNode(root.right, min);
					if (!rightOk) {
						isValid = false;
					}
				}
			}
		}
		return isValid;
	}

	public boolean isValidBST(TreeNode root) {
		if (root == null) {
			return false;
		}
		if (root.left != null) {
			if (root.val <= root.left.val) {
				return false;
			}
			boolean isValid = isValidBST(root.left);
			if (!isValid) {
				return false;
			}
			isValid = hasMaxNode(root.left, root.val);
			if (!isValid) {
				return false;
			}
		}
		if (root.right != null) {
			if (root.val >= root.right.val) {
				return false;
			}
			boolean isValid = isValidBST(root.right);
			if (!isValid) {
				return false;
			}
			isValid = hasMinNode(root.right, root.val);
			if (!isValid) {
				return false;
			}
		}
		return true;
	}
}
