package leetcode.topint;

import org.junit.Assert;
import org.junit.Test;

import leetcode.ListNode;

public class RemoveNthNodeFromEndOfList {
	@Test
	public void exampleOne() {
		ListNode data = ListNode.toList(new int [] {1, 2, 3, 4, 5});
		ListNode output = ListNode.toList(new int[] { 1, 2, 3, 5 });
		int n = 2;
		ListNode result = this.removeNthFromEnd(data, n);
		Assert.assertEquals(output,  result);
	}

	@Test
	public void exampleTwo() {
		ListNode data = ListNode.toList(new int [] {1});
		ListNode output = ListNode.toList(new int[] {});
		int n = 1;
		ListNode result = this.removeNthFromEnd(data, n);
		Assert.assertEquals(output,  result);
	}

	@Test
	public void exampleThree() {
		ListNode data = ListNode.toList(new int[] { 1, 2 });
		ListNode output = ListNode.toList(new int[] { 1 });
		int n = 1;
		ListNode result = this.removeNthFromEnd(data, n);
		Assert.assertEquals(output,  result);
	}

	public ListNode removeNthFromEnd(ListNode head, int n) {
		int currentPsn = 0;
		ListNode[] tracking = new ListNode[n];
		ListNode currentNode = head;
		while (currentNode != null) {
			tracking[currentPsn % n] = currentNode;
			if (currentNode.next != null) {
				currentPsn++;
			}
			currentNode = currentNode.next;
		}
		int trackPosition = currentPsn % n;
		System.out.println(trackPosition);
		System.out.println(tracking[trackPosition].val);
		if (tracking[trackPosition].next != null) {
			// tracking[trackPosition].val = tracking[trackPosition].next.val;
			tracking[trackPosition].next = tracking[trackPosition].next.next;
		}
		return head;
	}
}
