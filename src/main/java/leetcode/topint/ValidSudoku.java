package leetcode.topint;

import org.junit.Assert;
import org.junit.Test;

public class ValidSudoku {
	
	char [] convertStrArrToCharArr(String [] arr) {
		char [] output = new char[arr.length];
		for (int i = 0;i < arr.length;i++) {
			output[i] = arr[i].charAt(0);
		}
		return output;
	}
	
	char [][] convertStrArrToChrArr(String[][] arr) {
		char [][] outputArray = new char[arr.length][arr[0].length];
		for (int i = 0;i < arr.length;i++) {
			outputArray[i] = convertStrArrToCharArr(arr[i]);
		}
		return outputArray;
	}

	@Test
	public void exampleOne() {
		String [][] board = 
				{{"5","3",".",".","7",".",".",".","."}
				,{"6",".",".","1","9","5",".",".","."}
				,{".","9","8",".",".",".",".","6","."}
				,{"8",".",".",".","6",".",".",".","3"}
				,{"4",".",".","8",".","3",".",".","1"}
				,{"7",".",".",".","2",".",".",".","6"}
				,{".","6",".",".",".",".","2","8","."}
				,{".",".",".","4","1","9",".",".","5"}
				,{".",".",".",".","8",".",".","7","9"}};
		char [][] chrBoard = this.convertStrArrToChrArr(board);
		
		Assert.assertTrue(this.isValidSudoku(chrBoard));
	}
	
	@Test
	public void exampleTwo() {
		String [][] board = 
			{{"8","3",".",".","7",".",".",".","."}
			,{"6",".",".","1","9","5",".",".","."}
			,{".","9","8",".",".",".",".","6","."}
			,{"8",".",".",".","6",".",".",".","3"}
			,{"4",".",".","8",".","3",".",".","1"}
			,{"7",".",".",".","2",".",".",".","6"}
			,{".","6",".",".",".",".","2","8","."}
			,{".",".",".","4","1","9",".",".","5"}
			,{".",".",".",".","8",".",".","7","9"}};
		char [][] chrBoard = this.convertStrArrToChrArr(board);
		
		Assert.assertFalse(this.isValidSudoku(chrBoard));
	}
	
	private static final int BOARD_SIZE = 9;

	boolean isRowValid(int rowNum, char [][] board) {
		int used = 0;
		for (int i = 0; i < BOARD_SIZE; i++) {
			char valueAtIndex = board[rowNum][i];
			if (valueAtIndex == '.') {
				// skip it
			} else {
				int intValue = Integer.parseInt(Character.toString(valueAtIndex));
				int mask = (int)Math.pow(2.0, (double)intValue);
				if ((used & mask) > 0) {
					return false;
				} else {
					used = used | mask;
				}
			}
		}
		return true;
	}
	
	boolean isColumnValid(int colNum, char[][] board) {
		int used = 0;
		for (int i = 0; i < BOARD_SIZE; i++) {
			char valueAtIndex = board[i][colNum];
			if (valueAtIndex == '.') {
				// skip it
			} else {
				int intValue = Integer.parseInt(Character.toString(valueAtIndex));
				int mask = (int) Math.pow(2.0, (double) intValue);
				if ((used & mask) > 0) {
					return false;
				} else {
					used = used | mask;
				}
			}
		}
		return true;
	}

	boolean rowsValid(char[][] board) {
		for (int i = 0; i < BOARD_SIZE; i++) {
			if (!isRowValid(i, board)) {
				return false;
			}
		}
		return true;
	}

	boolean colsValid(char[][] board) {
		for (int i = 0; i < BOARD_SIZE; i++) {
			if (!isColumnValid(i, board)) {
				return false;
			}
		}
		return true;
	}

	boolean squaresValid(char[][] board) {
		int blockSize = (int) Math.sqrt(BOARD_SIZE);
		for (int i = 0; i < BOARD_SIZE; i++) {
			// our top-left index
			int row = (i / blockSize) * blockSize;
			int col = (i % blockSize) * blockSize;

			if (!checkSquareAt(blockSize, row, col, board)) {
				return false;
			}
		}
		return true;
	}

	private boolean checkSquareAt(int blockSize, int row, int col, char[][] board) {
		int used = 0;
		for (int r = 0; r < blockSize; r++) {
			for (int c = 0; c < blockSize; c++) {
				char valueAtIndex = board[r + row][c + col];
				if (valueAtIndex == '.') {
					// skip it
				} else {
					int intValue = Integer.parseInt(Character.toString(valueAtIndex));
					int mask = (int) Math.pow(2.0, (double) intValue);
					if ((used & mask) > 0) {
						return false;
					} else {
						used = used | mask;
					}
				}
			}
		}
		return true;
	}

	public boolean isValidSudoku(char[][] board) {
		if (!squaresValid(board)) {
			return false;
		}
		if (!rowsValid(board)) {
			return false;
		}
		if (!colsValid(board)) {
			return false;
		}
		return true;
    }
}
