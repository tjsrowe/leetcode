package leetcode.topint;

public class Utils {
	public static int[] convertStringToArray(final String input) {
		String [] parts = input.trim().substring(1, input.length()-1).split(",");
		int [] values = new int[parts.length];
		for (int i = 0;i < values.length;i++) {
			values[i] = Integer.parseInt(parts[i]);
		}
		return values;
	}
}
