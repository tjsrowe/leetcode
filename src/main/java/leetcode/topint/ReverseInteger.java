package leetcode.topint;

import org.junit.Assert;
import org.junit.Test;

public class ReverseInteger {
	@Test
	public void exampleOne() {
		int x = 123;
		int output = 321;

		int result = reverse(x);
		Assert.assertEquals(output, result);
		;
	}

	@Test
	public void exampleTwo() {
		int x = -123;
		int output = -321;

		int result = reverse(x);
		Assert.assertEquals(output, result);
	}

	@Test
	public void exampleThree() {
		int x = 120;
		int output = 21;

		int result = reverse(x);
		Assert.assertEquals(output, result);
		;
	}

	@Test
	public void exampleFour() {
		int x = 0;
		int output = 0;

		int result = reverse(x);
		Assert.assertEquals(output, result);
		;
	}

	@Test
	public void exampleFive() {
		int x = -7654321;
		int output = -1234567;

		int result = reverse(x);
		Assert.assertEquals(output, result);
	}

	@Test
	public void exampleSix() {
		int x = -654321;
		int output = -123456;

		int result = reverse(x);
		Assert.assertEquals(output, result);
	}

	@Test
	public void exampleSeven() {
		int x = 7654321;
		int output = 1234567;

		int result = reverse(x);
		Assert.assertEquals(output, result);
	}

	@Test
	public void exampleEight() {
		int x = 654321;
		int output = 123456;

		int result = reverse(x);
		Assert.assertEquals(output, result);
	}

	public int reverse(int x) {
		char[] strVal = Integer.toString(x).toCharArray();
		int idx = 0;
		int charNum = 0;
		int max = (strVal.length - 1) / 2;
		if (strVal[0] == '-') {
			max--;
			idx++;
		}
		while (charNum <= max) {
			char tmp = strVal[strVal.length - 1 - charNum];
			strVal[strVal.length - 1 - charNum] = strVal[idx];
			strVal[idx] = tmp;
			idx++;
			charNum++;
		}
		try {
			int output = Integer.parseInt(new String(strVal));
			return output;
		} catch (NumberFormatException ex) {
			return 0;
		}
	}
}
