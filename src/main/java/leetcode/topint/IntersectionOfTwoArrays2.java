package leetcode.topint;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

public class IntersectionOfTwoArrays2 {
	@Test
	public void exampleOne() {
		int[] nums1 = new int[] { 1, 2, 2, 1 };
		int[] nums2 = new int[] { 2, 2 };

		int[] output = new int[] { 2, 2 };
		int[] result = intersect(nums1, nums2);
		Assert.assertTrue(Arrays.equals(output, result));
	}

	public int[] intArr(Set<Integer> set) {
		int[] result = new int[set.size()];
		int index = 0;
		Iterator<Integer> iterator = set.iterator();
		while (iterator.hasNext()) {
			Integer nextValue = iterator.next();
			result[index++] = nextValue.intValue();
		}
		return result;
	}

	public int[] intersect(int[] nums1, int[] nums2) {
		Map<Integer, Integer> countMap1 = createCountMapFromArray(nums1);
		Map<Integer, Integer> countMap2 = createCountMapFromArray(nums2);

		Set<Integer> intersection = getIntersection(countMap1, countMap2);
		int[] result = intArr(intersection);
		return result;
	}

	void decreaseCountInMap(Map<Integer, Integer> map, Integer target) {
		Integer count = map.get(target);
		if (count != null && count == 0) {
			map.remove(target);
		}
		if (count != null) {
			count--;
			map.put(target, count);
		}
	}

	private Set<Integer> getIntersection(Map<Integer, Integer> countMap1, Map<Integer, Integer> countMap2) {
		Set<Integer> intersect = new HashSet<Integer>();
		for (Integer key : countMap1.keySet()) {
			removeOverlap(countMap1, countMap1, countMap2, intersect, key);
			removeOverlap(countMap2, countMap1, countMap2, intersect, key);
		}
		return intersect;
	}

	private void removeOverlap(Map<Integer, Integer> sourceMap, Map<Integer, Integer> countMap1,
			Map<Integer, Integer> countMap2, Set<Integer> intersect, Integer key) {
		Integer countIn1 = sourceMap.get(key);
		if (countIn1 != null) {
			for (int i = 0; i < countIn1; i++) {
				decreaseCountInMap(countMap1, key);
				decreaseCountInMap(countMap2, key);
				intersect.add(key);
			}
		}
	}

	private Map<Integer, Integer> createCountMapFromArray(int[] nums1) {
		Map<Integer, Integer> usage = new HashMap<Integer, Integer>();
		for (int i = 0; i < nums1.length; i++) {
			if (usage.containsKey(nums1[i])) {
				Integer existingCount = usage.get(nums1[i]);
				existingCount = existingCount + 1;
				usage.put(nums1[i], existingCount);
			} else {
				usage.put(nums1[i], 1);
			}
		}
		return usage;
	}
}
