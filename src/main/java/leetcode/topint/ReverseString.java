package leetcode.topint;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

public class ReverseString {
	@Test
	public void exampleOne() {
		final char[] input = "hello".toCharArray();
		final char[] output = "olleh".toCharArray();
		this.reverseString(input);
		Assert.assertTrue(Arrays.equals(output, input));
	}

	@Test
	public void exampleTwo() {
		final char[] input = "Hannah".toCharArray();
		final char[] output = "hannaH".toCharArray();

		this.reverseString(input);
		Assert.assertTrue(Arrays.equals(output, input));
	}

	@Test
	public void exampleThree() {
		final char[] input = { 'A', ' ', 'm', 'a', 'n', ',', ' ', 'a', ' ', 'p', 'l', 'a', 'n', ',', ' ', 'a', ' ', 'c',
				'a', 'n', 'a', 'l', ':', ' ', 'P', 'a', 'n', 'a', 'm', 'a' };
		final char[] output = { 'a', 'm', 'a', 'n', 'a', 'P', ' ', ':', 'l', 'a', 'n', 'a', 'c', ' ', 'a', ' ', ',',
				'n', 'a', 'l', 'p', ' ', 'a', ' ', ',', 'n', 'a', 'm', ' ', 'A' };

		this.reverseString(input);
		Assert.assertTrue(Arrays.equals(output, input));
	}

	public void reverseString(char[] s) {
		int max = (s.length - 1) / 2;
		for (int i = 0; i <= max; i++) {
			char tmp = s[s.length - i - 1];
			s[s.length - i - 1] = s[i];
			s[i] = tmp;
		}
	}
}
