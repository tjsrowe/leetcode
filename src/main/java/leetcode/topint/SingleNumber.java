package leetcode.topint;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

public class SingleNumber {
	@Test
	public void exampleOne() {
		int[] data = new int[] { 2, 2, 1 };
		Assert.assertEquals(1, this.singleNumber(data));
	}

	@Test
	public void exampleTwo() {
		int[] data = new int[] { 4, 1, 2, 1, 2 };
		Assert.assertEquals(4, this.singleNumber(data));
	}

	@Test
	public void exampleThree() {
		int[] data = new int[] { 1 };
		Assert.assertEquals(1, this.singleNumber(data));
	}

	public int singleNumber(int[] nums) {
		Set<Integer> hs = new HashSet<Integer>();

		for (int n : nums) {
			if (hs.contains(n)) {
				hs.remove(n);
			} else {
				hs.add(n);
			}
		}
		return hs.iterator().next();
	}
}
