package leetcode.topint;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

public class MoveZeros {
	@Test
	public void exampleOne() {
		int[] input = new int[] { 0, 1, 0, 3, 12 };
		int[] output = new int[] { 1, 3, 12, 0, 0 };
		this.moveZeroes(input);
		Assert.assertTrue(Arrays.equals(output, input));
	}

	@Test
	public void exampleTwo() {
		int[] input = new int[] { 0 };
		int[] output = new int[] { 0 };
		this.moveZeroes(input);
		Assert.assertTrue(Arrays.equals(output, input));
	}

	public void moveZeroes(int[] nums) {
		int availableIndex = 0;
		for (int index = 0; index < nums.length; index++) {
			nums[availableIndex] = nums[index];
			if (nums[index] != 0) {
				availableIndex++;
			}
		}
		while (availableIndex < nums.length) {
			nums[availableIndex++] = 0;
		}
	}
}
