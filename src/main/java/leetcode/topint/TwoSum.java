package leetcode.topint;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

public class TwoSum {
	@Test
	public void exampleOne() {
		int[] input = new int[] { 2, 7, 11, 15 };
		int target = 9;
		int[] output = new int[] { 0, 1 };
		int[] result = this.twoSum(input, target);
		Assert.assertTrue(Arrays.equals(output, result));
	}

	public int[] twoSum(int[] nums, int target) {
		Map<Integer, Integer> indexes = new HashMap<Integer, Integer>();
		int check;

		for (int i = 0; i < nums.length; i++) {
			check = target - nums[i];
			Integer valueNeeded = indexes.get(check);
			if (valueNeeded == null && !indexes.containsKey(check)) {
				indexes.put(nums[i], i);
			} else {
				int iv = valueNeeded.intValue();
				if (i != iv) {
					return new int[] { i, iv };
				}
			}
		}
		return null;
	}
}
