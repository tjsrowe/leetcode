package leetcode.topint;

import org.junit.Assert;
import org.junit.Test;

public class BestTimeToBuyAndSellStock2 {
	@Test
	public void testSetOne() {
		int[] data = new int[] { 7, 1, 5, 3, 6, 4 };
		int expected = 7;
		int maxProfit = this.maxProfit(data);
		Assert.assertEquals(expected, maxProfit);
	}

	@Test
	public void testSetTwo() {
		int[] data = new int[] { 1, 2, 3, 4, 5 };
		int expected = 4;
		int maxProfit = this.maxProfit(data);
		Assert.assertEquals(expected, maxProfit);
	}

	@Test
	public void testSetThree() {
		int[] data = new int[] { 7, 6, 4, 3, 1 };
		int expected = 0;
		int maxProfit = this.maxProfit(data);
		Assert.assertEquals(expected, maxProfit);
	}

	@Test
	public void testSetFour() {
		int[] data = new int[] { 2, 1, 2, 0, 1 };
		int expected = 2;
		int maxProfit = this.maxProfit(data);
		Assert.assertEquals(expected, maxProfit);
	}

	public int findNextPeak(int[] prices, int startingIndex) {
		int max = Integer.MIN_VALUE;
		int peakIndex = startingIndex;
		for (int i = startingIndex; i < prices.length; i++) {
			if (prices[i] > max) {
				max = prices[i];
				peakIndex = i;
			} else {
				break;
			}
		}
		return peakIndex;
	}

	public int maxProfit(int[] prices) {
		int profit = 0;

		for (int i = 0; i < prices.length; i++) {
			int nextPeak = findNextPeak(prices, i);
			if (nextPeak > i) {
				profit += prices[nextPeak] - prices[i];
				i = nextPeak;
			}
		}
		return profit;
	}
}
