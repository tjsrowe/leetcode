package leetcode.topint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

public class FirstUniqueCharacterInAString {
	@Test
	public void exampleOne() {
		final String s = "leetcode";
		final int output = 0;

		int result = firstUniqChar(s);
		Assert.assertEquals(result, output);
	}

	@Test
	public void exampleTwo() {
		final String s = "loveleetcode";
		final int output = 2;

		int result = firstUniqChar(s);
		Assert.assertEquals(result, output);
	}

	@Test
	public void exampleThree() {
		final String s = "aabb";
		final int output = -1;

		int result = firstUniqChar(s);
		Assert.assertEquals(result, output);
	}

	public int firstUniqChar(String s) {
		Map<Character, Integer> charsUsed = new HashMap<Character, Integer>();
		List<Character> order = new ArrayList<Character>();
		char[] chars = s.toCharArray();

		for (int i = 0; i < chars.length; i++) {
			char current = chars[i];
			if (charsUsed.containsKey(current)) {
				Integer index = charsUsed.get(current);
				index = -1;
				charsUsed.put(current, index);
			} else {
				order.add(current);
				charsUsed.put(current, i);
			}
		}
		for (int i = 0; i < order.size(); i++) {
			char check = order.get(i);
			Integer index = charsUsed.get(check);
			if (index != -1) {
				return index;
			}
		}
		return -1;
	}
}
