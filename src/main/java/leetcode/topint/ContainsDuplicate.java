package leetcode.topint;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

public class ContainsDuplicate {
	@Test
	public void exampleOne() {
		int[] data = new int[] { 1, 2, 3, 1 };
		Assert.assertTrue(this.containsDuplicate(data));
	}

	@Test
	public void exampleTwo() {
		int[] data = new int[] { 1, 2, 3, 4 };
		Assert.assertFalse(this.containsDuplicate(data));
	}

	@Test
	public void exampleThree() {
		int[] data = new int[] { 1, 1, 1, 3, 3, 4, 3, 2, 4, 2 };
		Assert.assertTrue(this.containsDuplicate(data));
	}

	public boolean containsDuplicate(int[] nums) {
		Set<Integer> hs = new HashSet<Integer>();

		for (int n : nums) {
			if (hs.contains(n)) {
				return true;
			}
			hs.add(n);
		}
		return false;
	}
}
