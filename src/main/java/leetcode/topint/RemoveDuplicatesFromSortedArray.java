package leetcode.topint;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

public class RemoveDuplicatesFromSortedArray {
	@Test
	public void removeOne() {
		int[] data = new int[] { 1, 1, 2 };
		int[] expected = new int[] { 1, 2, 0 };
		int blanks = this.removeDuplicates(data);
		Assert.assertEquals(2, blanks);
		Assert.assertTrue(Arrays.equals(expected, data));
	}

	@Test
	public void removeFive() {
		int [] data = new int[] { 0,0,1,1,1,2,2,3,3,4 };
		int [] expected = new int[] {0,1,2,3,4, 0, 0, 0, 0, 0};
		int blanks = this.removeDuplicates(data);
		Assert.assertEquals(5, blanks);
		Assert.assertTrue(Arrays.equals(expected, data));
	}

	public int removeDuplicates(int[] nums) {
		int returned = 0;
		for (int i = 1; i < nums.length; i++) {
			if (nums[i - 1] < nums[i]) {
				returned++;
			}
			nums[returned] = nums[i];
		}
		returned++;
		for (int i = returned; i < nums.length; i++) {
			nums[i] = 0;
		}
		return returned;
	}
}
