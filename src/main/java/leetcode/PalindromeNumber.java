package leetcode;

import org.junit.Assert;
import org.junit.Test;

public class PalindromeNumber {
	@Test
	public void exampleOne() {
		int x = 121;
		boolean output = true;
		Assert.assertEquals(output, this.isPalindrome(x));
	}

	@Test
	public void exampleTwo() {
		int x = -121;
		boolean output = false;
		Assert.assertEquals(output, this.isPalindrome(x));
	}

	@Test
	public void exampleThree() {
		int x = 10;
		boolean output = false;
		Assert.assertEquals(output, this.isPalindrome(x));
	}

	@Test
	public void exampleFour() {
		int x = -101;
		boolean output = false;
		Assert.assertEquals(output, this.isPalindrome(x));
	}

	@Test
	public void exampleFive() {
		int x = 1234554321;
		boolean output = true;
		Assert.assertEquals(output, this.isPalindrome(x));
	}

	@Test
	public void exampleSix() {
		int x = 123454321;
		boolean output = true;
		Assert.assertEquals(output, this.isPalindrome(x));
	}

	@Test
	public void exampleSeven() {
		int x = 1;
		boolean output = true;
		Assert.assertEquals(output, this.isPalindrome(x));
	}

	public boolean isPalindrome(int x) {
		final String strrep = Integer.toString(x);
		final char[] ch = strrep.toCharArray();

		int start = 0;
		int end = ch.length - 1;
		while (start <= end) {
			if (ch[start++] != ch[end--]) {
				return false;
			}
		}
		return true;
	}
}
