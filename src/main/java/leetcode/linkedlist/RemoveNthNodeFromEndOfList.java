package leetcode.linkedlist;

import org.junit.Assert;
import org.junit.Test;

import leetcode.ListNode;

public class RemoveNthNodeFromEndOfList {
	@Test
	public void exampleOne() {
		final ListNode head = ListNode.toList(new int[] {1, 2, 3, 4, 5});
		final int n = 2;
		final ListNode expectedOutput = ListNode.toList(new int [] {1, 2, 3, 5});
		final ListNode output = this.removeNthFromEnd(head, n);
		Assert.assertEquals(expectedOutput.toString(), output.toString());
	}
	
	@Test
	public void exampleTwo() {
		final ListNode head = ListNode.toList(new int[] {1});
		final int n = 1;
		final ListNode output = this.removeNthFromEnd(head, n);
		Assert.assertNull(output);
	}
	
	@Test
	public void exampleThree() {
		final ListNode head = ListNode.toList(new int[] {1, 2});
		final int n = 1;
		final ListNode expectedOutput = ListNode.toList(new int [] {1});
		final ListNode output = this.removeNthFromEnd(head, n);
		Assert.assertEquals(expectedOutput.toString(), output.toString());
	}
	
	@Test
	public void exampleFour() {
		final ListNode head = ListNode.toList(new int[] {1, 2});
		final int n = 2;
		final ListNode expectedOutput = ListNode.toList(new int [] {2});
		final ListNode output = this.removeNthFromEnd(head, n);
		Assert.assertEquals(expectedOutput.toString(), output.toString());
	}
	
	public ListNode removeNthFromEnd(ListNode head, int n) {
		ListNode firstPointer = head;
		ListNode secondPointer = null;
		
		while (n-- > 0) {
			if (firstPointer == null) {
				break;
			}
			firstPointer = firstPointer.next;
		}
		secondPointer = head;
		if (firstPointer == null) {
			return secondPointer.next;
		}
		while (firstPointer.next != null) {
			firstPointer = firstPointer.next;
			secondPointer = secondPointer.next;
		}
		secondPointer.next = secondPointer.next.next;
		return head;
	}
	
//    public ListNode removeNthFromEnd(ListNode head, int n) {
//        int currentPsn = 0;
//        ListNode [] tracking = new ListNode[n];
//        ListNode currentNode = head;
//        while (currentNode != null) {
//            tracking[currentPsn%n] = currentNode;
//            if (currentNode.next != null) {
//                currentPsn++;
//            }
//            currentNode = currentNode.next;
//        }
//        int trackPosition = currentPsn%n;
//        System.out.println(trackPosition);
//        System.out.println(tracking[trackPosition].val);
//        if (tracking[trackPosition].next != null) {
//            tracking[trackPosition].val = tracking[trackPosition].next.val;
//            tracking[trackPosition].next = tracking[trackPosition].next.next;
//        }
//        return head;
//    }
}
