package leetcode.linkedlist;

import java.util.HashSet;

import org.junit.Assert;
import org.junit.Test;

import leetcode.ListNode;

public class LinkedListCycle {
	@Test
	public void exampleOne() {
		int pos = 1;
		ListNode node = ListNode.toList(new int []{3, 2, 0, -4}, pos);
		boolean output = true;
		boolean result = this.hasCycle(node);
		Assert.assertEquals(output, result);
	}
	
	@Test
	public void exampleTwo() {
		int pos = 0;
		ListNode node = ListNode.toList(new int []{1, 2}, pos);
		boolean output = true;
		boolean result = this.hasCycle(node);
		Assert.assertEquals(output, result);
	}
	
	@Test
	public void exampleThree() {
		int pos = -1;
		ListNode node = ListNode.toList(new int []{1}, pos);
		boolean output = false;
		boolean result = this.hasCycle(node);
		Assert.assertEquals(output, result);
	}
	
	public boolean hasCycle(ListNode head) {
		if (head == null || head.next == null) {
			return false;
		}
		ListNode fast = head.next.next;
		ListNode slow = head;
		while (true) {
			if (fast == slow) {
				return true;
			}
			if (fast == null || fast.next == null) {
				return false;
			}
			fast = fast.next.next;
			slow = slow.next;
		}
	}
	
	public boolean hasCycleUsingHashSet(ListNode head) {
		HashSet<ListNode> nodes = new HashSet<ListNode>();
		while (head != null) {
			if (nodes.contains(head)) {
				return true;
			} else {
				nodes.add(head);
			}

			head = head.next;
            
		}
		return false;
	}
}
