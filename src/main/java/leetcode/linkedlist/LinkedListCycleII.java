package leetcode.linkedlist;

import org.junit.Assert;
import org.junit.Test;

import leetcode.ListNode;

public class LinkedListCycleII {
	@Test
	public void exampleOne() {
		int pos = 1;
		ListNode node = ListNode.toList(new int []{3, 2, 0, -4}, pos);
		ListNode target = node.get(pos);
		ListNode result = this.detectCycle(node);
		int targetPosition = node.getPosition(target);
		int resultPosition = node.getPosition(result);
		Assert.assertEquals(targetPosition, resultPosition);
		Assert.assertTrue(target == result);
	}
	
	@Test
	public void exampleTwo() {
		int pos = 0;
		ListNode node = ListNode.toList(new int []{1, 2}, pos);
		ListNode target = node.get(pos);
		ListNode result = this.detectCycle(node);
		int targetPosition = node.getPosition(target);
		int resultPosition = node.getPosition(result);
		Assert.assertEquals(targetPosition, resultPosition);
		Assert.assertTrue(target == result);
	}
	
	@Test
	public void exampleThree() {
		int pos = -1;
		ListNode node = ListNode.toList(new int []{1}, pos);
		ListNode target = null;
		ListNode result = this.detectCycle(node);
		int targetPosition = node.getPosition(target);
		int resultPosition = node.getPosition(result);
		Assert.assertEquals(targetPosition, resultPosition);
		Assert.assertTrue(target == result);
	}
	
	@Test
	public void exampleFour() {
		int pos = 24;
		ListNode node = ListNode.toList(new int []{-21,10,17,8,4,26,5,35,33,-7,-16,27,-12,6,29,-12,5,9,20,14,14,2,13,-24,21,23,-21,5}, pos);
		ListNode target = node.get(24);
		ListNode result = this.detectCycle(node);
		int targetPosition = node.getPosition(target);
		int resultPosition = node.getPosition(result);
		Assert.assertEquals(targetPosition, resultPosition);
		Assert.assertTrue(target == result);
	}
	
	public ListNode detectCycle(ListNode head) {
		if (head == null || head.next == null) {
			return null;
		}
		ListNode fast = head;
		ListNode slow = head;
		while (fast != null && fast.next != null) {
			fast = fast.next.next;
			slow = slow.next;
			if (fast == slow) {
				ListNode check = head;
				while (slow != check) {
					check = check.next;
					slow = slow.next;
				}
				return slow;
			}
		}
		return null;
	}
}
