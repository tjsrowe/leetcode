package leetcode;

import org.junit.Assert;
import org.junit.Test;

public class RemoveOutermostParentheses {
	@Test
	public void exampleOne() {
		final String s = "(()())(())";
		final String expectedOutput = "()()()";

		final String output = this.removeOuterParentheses(s);
		Assert.assertEquals(expectedOutput, output);
	}
	
	@Test
	public void exampleTwo() {
		final String s = "(()())(())(()(()))";
		final String expectedOutput = "()()()()(())";
		
		final String output = this.removeOuterParentheses(s);
		Assert.assertEquals(expectedOutput, output);
	}
	
	@Test
	public void exampleThree() {
		final String s = "()()";
		final String expectedOutput = "";
		
		final String output = this.removeOuterParentheses(s);
		Assert.assertEquals(expectedOutput, output);
	}
	
    public String removeOuterParentheses(String s) {
        int len = s.length();
        int depth = 0;
        StringBuilder buffer = new StringBuilder();
        StringBuilder output = new StringBuilder();
        for (int i = 0;i < len;i++) {
            char c = s.charAt(i);
            if (c == '(') {
                if (depth > 0) {
                    buffer.append(c);
                }
                depth++;
            } else if (c == ')') {
                depth--;
                if (depth > 0) {
                    buffer.append(c);
                }
            }
            if (depth == 0) {
                output.append(buffer.toString());
                buffer = new StringBuilder();
            }
        }
        output.append(buffer.toString());
        return output.toString();
    }
}
