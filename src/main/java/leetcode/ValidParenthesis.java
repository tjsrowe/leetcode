package leetcode;

import java.util.LinkedList;

import org.junit.Assert;
import org.junit.Test;

public class ValidParenthesis {
	@Test
	public void exampleOne() {
		final String s = "()";
		boolean expectedValue = true;
		Assert.assertEquals(expectedValue, this.isValid(s));
	}

	@Test
	public void exampleTwo() {
		final String s = "()[]{}";
		boolean expectedValue = true;
		Assert.assertEquals(expectedValue, this.isValid(s));
	}

	@Test
	public void exampleThree() {
		final String s = "(]";
		boolean expectedValue = false;
		Assert.assertEquals(expectedValue, this.isValid(s));
	}

	@Test
	public void exampleFour() {
		final String s = "([)]";
		boolean expectedValue = false;
		Assert.assertEquals(expectedValue, this.isValid(s));
	}

	@Test
	public void exampleFive() {
		final String s = "{[]}";
		boolean expectedValue = true;
		Assert.assertEquals(expectedValue, this.isValid(s));
	}

	public boolean isValid(String s) {
		char [] str = s.toCharArray();
		LinkedList<Character> stack = new LinkedList<Character>();
		for (int i = 0;i < str.length;i++) {
			char current = str[i];

			char peeked = '\0';
			if (stack.size() > 0) {
				peeked = stack.peekFirst();
			}
			if (current == ']') {
				if (peeked != '[') {
					return false;
				}
				stack.pop();
			}
			else if (current == '}') {
				if (peeked != '{') {
					return false;
				}
				stack.pop();
			}
			else if (current == ')') {
				if (peeked != '(') {
					return false;
				}
				stack.pop();
			}
			else {
				stack.push(current);
			}
		}
		if (stack.size() > 0) {
			return false;
		}
		return true;
	}
}
