package leetcode;

import org.junit.Assert;
import org.junit.Test;

public class ZigzagConversion {
	@Test
	public void exampleOne() {
		String s = "PAYPALISHIRING";
		int numRows = 3;
		String expectedOutput = "PAHNAPLSIIGYIR";
		
		String output = this.convert(s, numRows);
		Assert.assertEquals(expectedOutput,  output);
	}
	
	@Test
	public void exampleTwo() {
		String s = "PAYPALISHIRING";
		int numRows = 4;
		String expectedOutput = "PINALSIGYAHRPI";
		
		String output = this.convert(s, numRows);
		Assert.assertEquals(expectedOutput,  output);
	}
	
	@Test
	public void exampleThree() {
		String s = "A";
		int numRows = 1;
		String expectedOutput = "A";
		
		String output = this.convert(s, numRows);
		Assert.assertEquals(expectedOutput,  output);
	}
	
    public String convert(String s, int numRows) {
        int modulus = (2*numRows)-2;
        StringBuilder sb = new StringBuilder();
        int strlen = s.length();

        for (int rownum = 0;rownum < numRows;rownum++) {
        	for (int j = 0;j < strlen;j++) {
        		if (numRows == 1 ||
        				(rownum == 0 && (modulus == 0 || j % modulus == 0)) ||
        				(j % modulus == rownum || j % modulus == (modulus - rownum)) ||
        				(rownum == numRows - 1 && j % modulus == numRows-1)
        		) {
        			sb.append(s.charAt(j));
        		}
        	}
        }

        final String output = sb.toString();
        return output;
    }
}
