package leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

public class FindTheUsersActiveMinutes {
	@Test
	public void exampleOne() {
		final int [][] logs = {
			{0,5},{1,2},{0,2},{0,5},{1,3}
		};
		final int k = 5;
		final int [] expectedOutput = {0, 2, 0, 0, 0};
		final int [] output = this.findingUsersActiveMinutes(logs, k);
		Assert.assertArrayEquals(expectedOutput, output);
	}
	
	@Test
	public void exampleTwo() {
		final int [][] logs = {
				{1,1},{2,2},{2,3}
		};
		final int k = 4;
		final int [] expectedOutput = {1, 1, 0, 0};
		final int [] output = this.findingUsersActiveMinutes(logs, k);
		Assert.assertArrayEquals(expectedOutput, output);
	}
	
	@Test
	public void exampleThree() {
		final int [][] logs = {
				{0,5},{1,2},{0,2}, {0,5}, {1,3}
		};
		final int k = 5;
		final int [] expectedOutput = {0, 2, 0, 0, 0};
		final int [] output = this.findingUsersActiveMinutes(logs, k);
		Assert.assertArrayEquals(expectedOutput, output);
	}
	
    public int[] findingUsersActiveMinutes(int[][] logs, int k) {
        Map<Integer, List<Integer>> userUsage = new HashMap<Integer, List<Integer>>();
        
        addUsage(logs, userUsage);
        int[] output = buildOutput(k, userUsage);
        return output;
    }

	private int[] buildOutput(int k, Map<Integer, List<Integer>> userUsage) {
		int [] output = new int[k];
        Set<Integer> keys = userUsage.keySet();
        for (Integer userId : keys) {
        	List<Integer> userEntries = userUsage.get(userId);
        	int entries = userEntries.size();
        	if (entries > 0) {
        		output[entries-1] = output[entries-1] + 1;
        	}
        }
		return output;
	}

	private void addUsage(int[][] logs, Map<Integer, List<Integer>> userUsage) {
		for (int i = 0;i < logs.length;i++) {
        	int [] logEntry = logs[i];
        	int user = logEntry[0];
        	int eventTime = logEntry[1];
        	
        	List<Integer> currentUserUsage = null;
        	if (!userUsage.containsKey(user)) {
        		currentUserUsage = new ArrayList<Integer>();
        		userUsage.put(user, currentUserUsage);
        	} else {
        		currentUserUsage = userUsage.get(user);
        	}
        	if (!currentUserUsage.contains(eventTime)) {
        		currentUserUsage.add(eventTime);
        	}
        }
	}
}
