package leetcode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class BinaryTreeLevelOrderTraversal {
	@Test
	public void exampleOne() {
		TreeNode data = TreeNode.parseString("[3,9,20,null,null,15,7]");
		List<List<Integer>> levelOrder = levelOrder(data);
		String output = levelOrder.toString().replace(" ", "");
		Assert.assertEquals("[[3],[9,20],[15,7]]", output);
	}

	@Test
	public void exampleTwo() {
		TreeNode data = TreeNode.parseString("[1]");
		List<List<Integer>> levelOrder = levelOrder(data);
		String output = levelOrder.toString().replace(" ", "");
		Assert.assertEquals("[[1]]", output);
	}

	@Test
	public void exampleThree() {
		TreeNode data = TreeNode.parseString("[]");
		List<List<Integer>> levelOrder = levelOrder(data);
		String output = levelOrder.toString().replace(" ", "");
		Assert.assertEquals("[]", output);
	}

	public List<List<Integer>> levelOrder(TreeNode root) {
		LinkedList<TreeNode> stack = new LinkedList<TreeNode>();
		if (root != null) {
			stack.addFirst(root);
		}

		List<List<Integer>> output = new ArrayList<List<Integer>>();
		int elementCount = stack.size();
		while (elementCount > 0) {
			List<Integer> elements = new ArrayList<Integer>();
			for (int i = 0; i < elementCount; i++) {
				TreeNode stackTop = stack.pollFirst();
				elements.add(stackTop.val);

				if (stackTop.left != null) {
					stack.addLast(stackTop.left);
				}
				if (stackTop.right != null) {
					stack.addLast(stackTop.right);
				}
			}
			output.add(elements);
			elementCount = stack.size();
		}
		return output;
	}
}
