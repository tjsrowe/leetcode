package leetcode;

import org.junit.Assert;
import org.junit.Test;

public class CountUnhappyFriends {
	@Test
	public void exampleOne() {
		final int n = 4;
		final int[][] preferences = {{1, 2, 3}, {3, 2, 0}, {3, 1, 0}, {1, 2, 0}};
		final int[][] pairs = {{0, 1}, {2, 3}};
		final int expectedOutput = 2;
		
		final int output = unhappyFriends(n, preferences, pairs);
		Assert.assertEquals(expectedOutput, output);
	}

	
    public int partnerPreference(int person, int partner, int[][] preferences) {
        int [] preferenceList = preferences[person];
        for (int i = 0;i < preferenceList.length;i++) {
            if (preferenceList[i] == partner) {
                return preferenceList[i];
            }
        }
        return -1;
    }
    
    public int unhappyFriends(int n, int[][] preferences, int[][] pairs) {
        int [] partners = new int[n];
        int [] partnerPrefs = new int[n];
        int [][] partnerScores = new int[n][n];
//        for (int )
        
        for (int i = 0;i < pairs.length;i++) {
            int person = pairs[i][0];
            int partner = pairs[i][1];
            partners[person] = partner;
            partnerPrefs[person] = partnerPreference(person, partner, preferences);

            person = pairs[i][1];
            partner = pairs[i][0];
            partners[person] = partner;
            partnerPrefs[person] = partnerPreference(person, partner, preferences);
        }
        
        for (int i = 0;i < n;i++) {
//            if (partnerPrefs[i])
        }
        return -1;
    }
}
