package leetcode.arrays;

import java.util.HashSet;

import org.junit.Assert;
import org.junit.Test;

public class CheckIfNAndItsDoubleExist {
	@Test
	public void exampleOne() {
		int[] nums = new int[] { 10, 2, 5, 3 };
		boolean output = true;
		Assert.assertEquals(output, this.checkIfExist(nums));
	}
	
	@Test
	public void exampleTwo() {
		int[] nums = new int[] { 7, 1, 14, 11};
		boolean output = true;
		Assert.assertEquals(output, this.checkIfExist(nums));
	}
	
	@Test
	public void exampleThree() {
		int[] nums = new int[] { 3, 1, 7, 11 };
		boolean output = false;
		Assert.assertEquals(output, this.checkIfExist(nums));
	}
	
	@Test
	public void exampleFour() {
		int[] nums = new int[] { 4, -7, 11, 4, 18 };
		boolean output = false;
		Assert.assertEquals(output, this.checkIfExist(nums));
	}
	
    public boolean checkIfExist(int[] arr) {
    	HashSet<Integer> checks = new HashSet<Integer>();
    	for (int i = 0;i < arr.length;i++) {
    		int val = arr[i];
    		int valDouble = val * 2; 
    		if (checks.contains(valDouble)) {
    			return true;
    		}
    		if (val % 2 == 0) {
    			int half = val/2;
    			if (checks.contains(half)) {
    				return true;
	    		}
    		}
    		checks.add(val);
    	}
    	return false;
    }
}
