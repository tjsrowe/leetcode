package leetcode.arrays;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

public class RemoveElement {
	@Test
	public void exampleOne() {
		int[] nums = new int[] { 3, 2, 2, 3 };
		int val = 3;
		int output = this.removeElement(nums, val);
		int[] expectedResult = new int[] { 2, 2 };
		int[] target = new int[output];
		System.arraycopy(nums, 0, target, 0, output);
		Assert.assertTrue(Arrays.equals(expectedResult, target));
	}

	@Test
	public void exampleTwo() {
		int[] nums = new int[] { 0, 1, 2, 2, 3, 0, 4, 2 };
		int val = 2;
		int output = this.removeElement(nums, val);
		int[] expectedResult = new int[] { 0, 1, 3, 0, 4 };
		int[] target = new int[output];
		System.arraycopy(nums, 0, target, 0, output);
		Assert.assertTrue(Arrays.equals(expectedResult, target));
	}

	public int removeElement(int[] nums, int val) {
		int numRemoved = 0;
		for (int i = 0; i < nums.length; i++) {
			if (nums[i] == val) {
				numRemoved++;
			} else if (numRemoved > 0) {
				nums[i - numRemoved] = nums[i];
			}
		}
		return nums.length - numRemoved;
	}
}
