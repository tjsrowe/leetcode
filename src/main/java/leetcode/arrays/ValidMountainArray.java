package leetcode.arrays;

import org.junit.Assert;
import org.junit.Test;

public class ValidMountainArray {
	@Test
	public void exampleOne() {
		int [] arr = {2, 1};
		boolean output = false;
		boolean result = this.validMountainArray(arr);
		Assert.assertEquals(output, result);
	}
	
	@Test
	public void exampleTwo() {
		int [] arr = {3, 5, 5};
		boolean output = false;
		boolean result = this.validMountainArray(arr);
		Assert.assertEquals(output, result);
	}
	
	@Test
	public void exampleThree() {
		int [] arr = {0, 3, 2, 1};
		boolean output = true;
		boolean result = this.validMountainArray(arr);
		Assert.assertEquals(output, result);
	}
	
	@Test
	public void exampleFour() {
		int [] arr = {1, 3, 2};
		boolean output = true;
		boolean result = this.validMountainArray(arr);
		Assert.assertEquals(output, result);
	}
	
	@Test
	public void exampleFive() {
		int [] arr = {2};
		boolean output = false;
		boolean result = this.validMountainArray(arr);
		Assert.assertEquals(output, result);
	}
	
	@Test
	public void exampleSix() {
		int [] arr = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
		boolean output = false;
		boolean result = this.validMountainArray(arr);
		Assert.assertEquals(output, result);
	}
	
	@Test
	public void exampleSeven() {
		int [] arr = {0, 1, 2, 1, 2};
		boolean output = false;
		boolean result = this.validMountainArray(arr);
		Assert.assertEquals(output, result);
	}
	
    public boolean validMountainArray(int[] arr) {
    	boolean ascending = true;
    	if (arr.length <= 2) {
    		return false;
    	}
        for (int i = 1;i < arr.length;i++) {
        	if (arr[i - 1] == arr[i]) {
        		return false;
        	}
        	if (arr[i - 1] > arr[i]) {
        		if (ascending && i == 1) {
        			return false;
        		} else if (ascending) {
        			ascending = false;
        		}
        	} else if (!ascending) {
        		return false;
        	}
        }
        return !ascending;
    }
}
