package leetcode;

import org.junit.Assert;
import org.junit.Test;

public class PathSum {
	@Test
	public void exampleOne() {
		TreeNode data = TreeNode.parseString("[5,4,8,11,null,13,4,7,2,null,null,null,1]");
		int targetSum = 22;

		Assert.assertTrue(this.hasPathSum(data, targetSum));
	}

	@Test
	public void exampleTwo() {
		TreeNode data = TreeNode.parseString("[1, 2, 3]");
		int targetSum = 5;

		Assert.assertFalse(this.hasPathSum(data, targetSum));
	}

	@Test
	public void exampleThree() {
		TreeNode data = TreeNode.parseString("[1, 2]");
		int targetSum = 0;

		Assert.assertFalse(this.hasPathSum(data, targetSum));
	}

	@Test
	public void exampleFour() {
		TreeNode data = TreeNode.parseString("[1, 2]");
		int targetSum = 1;

		Assert.assertFalse(this.hasPathSum(data, targetSum));
	}

	public boolean hasPathSum(TreeNode root, int targetSum) {
		if (root == null) {
			return false;
		}
		if (root.val == targetSum && root.left == null && root.right == null) {
			return true;
		}
		if (root.left != null && hasPathSum(root.left, targetSum - root.val)) {
			return true;
		}
		if (root.right != null && hasPathSum(root.right, targetSum - root.val)) {
			return true;
		}
		return false;
	}
}
