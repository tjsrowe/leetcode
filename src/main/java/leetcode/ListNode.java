package leetcode;

public class ListNode {
	public int val;
	public ListNode next;

	ListNode() {
	}

	ListNode(int val) {
		this.val = val;
	}

	ListNode(int val, ListNode next) {
		this.val = val;
		this.next = next;
	}

	public String toString() {
		return this.val + "->" + (this.next != null ? this.next.toString() : "NULL");
	}

	public static void addCycleTo(ListNode head, int loopPosition) {
		int currentPosition = 0;

		ListNode loop = null;
		while (true) {
			if (currentPosition == loopPosition) {
				loop = head;
			}
			if (head.next == null) {
				head.next = loop;
				break;
			}
			head = head.next;
			currentPosition++;
		}
	}
	
	public static ListNode toList(int nums[], int loopTo) {
		ListNode head = toList(nums);
		if (loopTo >= 0) {
			addCycleTo(head, loopTo);
		}
		return head;
	}
	
	public static ListNode toList(int nums[]) {
		ListNode prev = new ListNode();
		ListNode head = null;
		for (int i = 0; i < nums.length; i++) {
			ListNode current = new ListNode();
			current.val = nums[i];
			if (head == null) {
				head = current;
				prev = current;
			} else {
				prev.next = current;
				prev = current;
			}
		}
		return head;
	}

	public static void print(ListNode node) {
		if (node != null) {
			System.out.println(Integer.toString(node.val));
			print(node.next);
		}
	}

	public boolean equals(ListNode other) {
		if (other == null) {
			return false;
		}
		String otherStr = other.toString();
		String thisStr = this.toString();
		return thisStr.equals(otherStr);
	}
	
	public ListNode get(int index) {
		ListNode current = this;
		for (int i = 0;i < index && current != null;i++) {
			current = current.next;
		}
		return current;
	}
	
	public int getPosition(ListNode target) {
		ListNode current = this;
		int psn = 0;
		while (true) {
			if (current == target) {
				return psn;
			} else if (current == null) {
				return -1;
			}
			current = current.next;
			psn++;
		}
	}
}
