package leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class BinaryTreePostorderTraversal {
	@Test
	public void exampleOne() {
		TreeNode data = TreeNode.parseString("[1,null,2,3]");
		List<Integer> output = this.postorderTraversal(data);

		Assert.assertEquals(Arrays.asList(3, 2, 1), output);
	}

	@Test
	public void exampleTwo() {
		TreeNode data = TreeNode.parseString("[]");
		List<Integer> output = this.postorderTraversal(data);

		Assert.assertEquals(Arrays.asList(), output);
	}

	@Test
	public void exampleThree() {
		TreeNode data = TreeNode.parseString("[1]");
		List<Integer> output = this.postorderTraversal(data);

		Assert.assertEquals(Arrays.asList(1), output);
	}

	@Test
	public void exampleFour() {
		TreeNode data = TreeNode.parseString("[1,2]");
		List<Integer> output = this.postorderTraversal(data);

		Assert.assertEquals(Arrays.asList(2, 1), output);
	}

	@Test
	public void exampleFive() {
		TreeNode data = TreeNode.parseString("[1,null,2]");
		List<Integer> output = this.postorderTraversal(data);

		Assert.assertEquals(Arrays.asList(2, 1), output);
	}

	public List<Integer> postorderTraversal(TreeNode root) {
		List<Integer> output = new ArrayList<Integer>();

		if (root != null) {
			if (root.left != null) {
				List<Integer> left = postorderTraversal(root.left);
				output.addAll(left);
			}
			if (root.right != null) {
				List<Integer> right = postorderTraversal(root.right);
				output.addAll(right);
			}
			output.add(root.val);
		}

		return output;
	}

}
