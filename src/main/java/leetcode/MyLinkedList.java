package leetcode;

class MyLinkedList {
	class ListNode {
		int val;
		ListNode next;

		ListNode(int v) {
			this.val = v;
		}

		public String toString() {
			String output = Integer.toString(this.val);
			if (this.next != null) {
				output = output + "->" + this.next.toString();
			}
			return output;
		}
	}

	ListNode head = null;

	public MyLinkedList() {

	}

	public int get(int index) {
		ListNode current = this.head;
		for (int i = 0; i < index && current != null; i++) {
			current = current.next;
		}
		if (current == null) {
			return -1;
		}
		return current.val;
	}

	public void addAtHead(int val) {
		ListNode existingHead = this.head;
		this.head = new ListNode(val);
		head.val = val;
		head.next = existingHead;
	}

	public void addAtTail(int val) {
		ListNode newNode = new ListNode(val);
		if (this.head == null) {
			this.head = newNode;
		} else {
			ListNode tail = this.head;
			while (tail.next != null) {
				tail = tail.next;
			}
			tail.next = newNode;
		}
	}

	public void addAtIndex(int index, int val) {
		if (this.head == null) {
			if (index == 0) {
				this.head = new ListNode(val);
			}
			return;
		}

		ListNode current = this.head;
		ListNode prev = this.head;
		for (int i = 0; i < index && current != null; i++) {
			prev = current;
			current = current.next;
		}
		if (current == null) {
			prev.next = new ListNode(val);
		} else {
			ListNode newNext = new ListNode(current.val);
			newNext.next = current.next;
			current.val = val;
			current.next = newNext;
		}
	}

	public void deleteAtIndex(int index) {
		if (index == 0) {
			if (head != null) {
				head = head.next;
			}
			return;
		}

		int currentIndex = 0;
		ListNode current = head;
		ListNode prev = null;
		while (currentIndex < index && current != null) {
			prev = current;
			current = current.next;
			currentIndex++;
		}
		if (currentIndex == index && current != null) {
			if (current == head) {
				head = head.next;
			} else if (current.next == null) {
				prev.next = null;
			} else {
				current.val = current.next.val;
				current.next = current.next.next;
			}
		}
	}

	public String toString() {
		return head != null ? head.toString() : "null";
	}
}

/**
 * Your MyLinkedList object will be instantiated and called as such:
 * MyLinkedList obj = new MyLinkedList(); int param_1 = obj.get(index);
 * obj.addAtHead(val); obj.addAtTail(val); obj.addAtIndex(index,val);
 * obj.deleteAtIndex(index);
 */