package leetcode;

import org.junit.Assert;
import org.junit.Test;

public class ImplementStrStr {
	@Test
	public void exampleOne() {
		final String haystack = "hello";
		final String needle = "ll";
		final int output = 2;
		Assert.assertEquals(output, this.strStr(haystack, needle));
	}

	@Test
	public void exampleTwo() {
		final String haystack = "aaaaa";
		final String needle = "bba";
		final int output = -1;
		Assert.assertEquals(output, this.strStr(haystack, needle));
	}

	@Test
	public void exampleThree() {
		final String haystack = "";
		final String needle = "";
		final int output = 0;
		Assert.assertEquals(output, this.strStr(haystack, needle));
	}

	public static final boolean matchesAtIndex(char[] haystack, int index, char[] needle) {
		for (int i = 0; i < needle.length; i++) {
			if (haystack[index + i] != needle[i]) {
				return false;
			}
		}
		return true;
	}

	public int strStr(String haystack, String needle) {
		if ("".equals(needle)) {
			return 0;
		}

		char[] hs = haystack.toCharArray();
		char[] n = needle.toCharArray();

		int maxStartIndex = hs.length - n.length;
		for (int i = 0; i <= maxStartIndex; i++) {
			if (matchesAtIndex(hs, i, n)) {
				return i;
			}
		}
		return -1;
	}
}
