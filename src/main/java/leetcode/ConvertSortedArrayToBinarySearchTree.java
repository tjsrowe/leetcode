package leetcode;

import org.junit.Assert;
import org.junit.Test;

public class ConvertSortedArrayToBinarySearchTree {
	@Test
	public void exampleOne() {
		// TreeNode data = TreeNode.parseString("[-10,-3,0,5,9]");
		// boolean isValid = this.isValidBST(data);
		// Assert.assertFalse(isValid);

	}

	@Test
	public void copyPaddedArray() {
		int[] data = new int[] { 1, 2, 1, 4, 5 };
		int[] output = createPaddedArray(7, data);
		Assert.assertEquals(7, output.length);
	}

	static int[] createPaddedArray(int maxSize, int[] nums) {
		int[] output = new int[maxSize];
		System.arraycopy(nums, 0, output, maxSize - nums.length, nums.length);
		return output;
	}

	static int[] convertArrayToHeap(int[] array) {
		int[] output = new int[array.length];
		return output;
	}

	public TreeNode sortedArrayToBST(int[] nums) {
		int height = 0;
		int size = nums.length;
		int maxNodes = 0;
		while (size > 0) {
			height++;
			size = size >> 1;
			maxNodes = (maxNodes << 1) + 1;
		}

		int[] padded = createPaddedArray(maxNodes, nums);

		// TreeNode node = buildNodeFromArray(nums, index, height);
		return null;
	}
}
