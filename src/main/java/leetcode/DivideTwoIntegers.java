package leetcode;

import java.util.LinkedList;

import org.junit.Assert;
import org.junit.Test;

public class DivideTwoIntegers {
	@Test
	public void exampleOne() {
		final int dividend = 10;
		final int divisor = 3;
		final int expectedOutput = 3;
		final int output = this.divide(dividend, divisor);
		Assert.assertEquals(expectedOutput, output);
	}

	@Test
	public void exampleTwo() {
		final int dividend = 7;
		final int divisor = -3;
		final int expectedOutput = -2;
		final int output = this.divide(dividend, divisor);
		Assert.assertEquals(expectedOutput, output);
	}
	
	@Test
	public void exampleThree() {
		final int dividend = 0;
		final int divisor = 1;
		final int expectedOutput = 0;
		final int output = this.divide(dividend, divisor);
		Assert.assertEquals(expectedOutput, output);
	}
	
	@Test
	public void exampleFour() {
		final int dividend = 1;
		final int divisor = 1;
		final int expectedOutput = 1;
		final int output = this.divide(dividend, divisor);
		Assert.assertEquals(expectedOutput, output);
	}
	
	@Test
	public void exampleFive() {
		final int dividend = -1;
		final int divisor = 1;
		final int expectedOutput = -1;
		final int output = this.divide(dividend, divisor);
		Assert.assertEquals(expectedOutput, output);
	}
	
	@Test
	public void exampleSix() {
		final int dividend = -2147483648;
		final int divisor = -1;
		final int expectedOutput = 214748367;
		final int output = this.divide(dividend, divisor);
		Assert.assertEquals(expectedOutput, output);
	}
	
	@Test
	public void exampleSeven() {
		final int dividend = -2147483648;
		final int divisor = 1;
		final int expectedOutput = -2147483648;
		final int output = this.divide(dividend, divisor);
		Assert.assertEquals(expectedOutput, output);
	}
	
//	public int divide2(int dividend, int divisor) {
//		if (divisor > dividend) {
//			return 0;
//		}
//		int count = divide(dividend, divisor + divisor);
//		if (count > 0) {
//			return count
//		}
//	}
	
	public int divide(int dividend, int divisor) {
		int count = 0;
		int total = 0;
		boolean invert = false;
		
		// special case
		if (dividend == Integer.MIN_VALUE && divisor == 1) {
			return Integer.MIN_VALUE;
		}
		if (dividend == Integer.MIN_VALUE) {
			dividend = Integer.MAX_VALUE;
			invert = !invert;
		} else if (dividend < 0) {
			dividend = 0 - dividend;
			invert = !invert;
		}
		if (divisor < 0) {
			invert = !invert;
			divisor = 0 - divisor;
		}

		LinkedList<int[]> additions = new LinkedList<int[]>();
		additions.add(new int[]{1, divisor});

		while (total <= dividend) {
			int [] top = additions.peekLast();
			
			while (top != null && total + top[1] < dividend) {
				int [] newtop = new int[] {top[0] + top[0], top[1]+top[1]};
				additions.push(newtop);
				total += top[1];
				count += top[0];
				top = newtop;
			}
			if (total + divisor == dividend) {
				count++;
				break;
			} else {
				while (top != null) {
					while (top != null && total + top[1] < 0) {
						top = null;
						if (additions.size() > 0) {
							top = additions.pop();
						}
					}
					while (top != null && total + top[1] <= dividend && total + top[1] > 0) {
						total += top[1];
						count += top[0];						
					}
					if (additions.size() > 0) {
						top = additions.pop();
					} else {
						top = null;
					}
				}
				break;
			}
		}
		if (invert) {
			count = 0-count;
		}
		return count;
	}
}
