package leetcode;

public class SumOfLeftLeaves {
    public static boolean isLeaf(TreeNode node) {
        return node.left == null && node.right == null;
    }
    public int sumOfLeftLeaves(TreeNode root) {
        int sum = 0;
        if (root == null) {
            return 0;
        }
        if (root.left != null && isLeaf(root.left)) {
            sum += root.left.val;
        } else {
            sum += sumOfLeftLeaves(root.left);
        }
        int sumRight = sumOfLeftLeaves(root.right);
        sum += sumRight;
        return sum;
    }
}
