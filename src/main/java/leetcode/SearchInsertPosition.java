package leetcode;

import org.junit.Assert;
import org.junit.Test;

public class SearchInsertPosition {
	@Test
	public void exampleOne() {
		int[] nums = new int[] { 1, 3, 5, 6 };
		int target = 5;
		int expectedOutput = 2;
		int output = this.searchInsert(nums, target);
		Assert.assertEquals(expectedOutput, output);
	}

	@Test
	public void exampleTwo() {
		int[] nums = new int[] { 1, 3, 5, 6 };
		int target = 2;
		int expectedOutput = 1;
		int output = this.searchInsert(nums, target);
		Assert.assertEquals(expectedOutput, output);
	}

	@Test
	public void exampleThree() {
		int[] nums = new int[] { 1, 3, 5, 6 };
		int target = 7;
		int expectedOutput = 4;
		int output = this.searchInsert(nums, target);
		Assert.assertEquals(expectedOutput, output);
	}

	@Test
	public void exampleFour() {
		int[] nums = new int[] { 1, 3, 5, 6 };
		int target = 0;
		int expectedOutput = 0;
		int output = this.searchInsert(nums, target);
		Assert.assertEquals(expectedOutput, output);
	}

	@Test
	public void exampleFive() {
		int[] nums = new int[] { 1 };
		int target = 0;
		int expectedOutput = 0;
		int output = this.searchInsert(nums, target);
		Assert.assertEquals(expectedOutput, output);
	}

	@Test
	public void exampleSix() {
		int[] nums = new int[] { 1, 2, 3, 4, 5, 10 };
		int target = 2;
		int expectedOutput = 1;
		int output = this.searchInsert(nums, target);
		Assert.assertEquals(expectedOutput, output);
	}

	public int binarySearch(int[] nums, int target, int left, int right) {
		if (nums[left] == target) {
			return left;
		}
		if (left == right && (nums[left] <= target || nums[right] >= target)) {
			return left;
		}
		if (nums[left] > target) {
			return -1;
		}
		if (nums[right] < target) {
			return -1;
		}

		if (nums[left] < target) {
			int newLeft = left + ((right - left) / 2);
			if (newLeft == left) {
				newLeft++;
			}
			int result = binarySearch(nums, target, newLeft, right);
			if (result > -1) {
				return result;
			}
			int newRight = left + ((right - left) / 2);
			if (newRight == right) {
				newRight--;
			}
			return binarySearch(nums, target, left, newRight);
		} else {
			int newRight = left + ((right - left) / 2);
			if (newRight == right) {
				newRight--;
			}
			int result = binarySearch(nums, target, left, newRight);
			if (result > -1) {
				return result;
			}
			int newLeft = left + ((right - left) / 2);
			if (newLeft == left) {
				newLeft++;
			}
			return binarySearch(nums, target, newLeft, right);
		}
	}

	public int searchInsert(int[] nums, int target) {
		int left = 0;
		int right = nums.length - 1;
		if (nums[0] > target) {
			return 0;
		}
		if (nums[nums.length - 1] < target) {
			return nums.length;
		}

		return binarySearch(nums, target, left, right);
	}
}
