package leetcode.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ResourceUtils {
	public static int [] loadIntArrayResource(String file) throws IOException {
		InputStream stream = ResourceUtils.class.getResourceAsStream(file);
		InputStreamReader sr = new InputStreamReader(stream);
		BufferedReader br = new BufferedReader(sr);
		String line = br.readLine();
		List<Integer> ints = new ArrayList<Integer>();
		String remaining = "";
		while (line != null) {
			line = remaining + line;
			while (line.contains(",")) {
				int cindex = line.indexOf(",");
				String part = line.substring(0, cindex);
				if (part.startsWith("[")) {
					part = part.substring(1);
				}
				int val = Integer.parseInt(part.trim());
				ints.add(val);
				line = line.substring(cindex+1);
			}
			remaining = line;
			line = br.readLine();
		}
		if (remaining.trim().endsWith("]")) {
			String withoutEnd = remaining.substring(0, remaining.indexOf(']'));
			int last = Integer.parseInt(withoutEnd.trim());
			ints.add(last);
		}
		int[] primitiveInts = ints.stream().mapToInt(i->i).toArray();
		return primitiveInts;
	}
}
