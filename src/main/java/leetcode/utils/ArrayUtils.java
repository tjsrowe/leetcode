package leetcode.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ArrayUtils {
	public static List<List<Integer>> twoDimensionToList(int [][] nums) {
		List<List<Integer>> list = new ArrayList<List<Integer>>();
		for (int i = 0;i < nums.length;i++) {
			List<Integer> l2 = Arrays.stream(nums[i]).boxed().collect(Collectors.toList());
			list.add(l2);
		}
		return list;
	}
}
