package leetcode;

import org.junit.Assert;
import org.junit.Test;

public class SearchInRotatedSortedArray {
	@Test
	public void exampleOne() {
		final int[] nums = { 4, 5, 6, 7, 0, 1, 2 };
		final int target = 0;
		final int expectedOutput = 4;
		final int output = search(nums, target);
		Assert.assertEquals(expectedOutput, output);
	}

	@Test
	public void exampleOneA() {
		final int[] nums = { 4, 5, 6, 7, 0, 1, 2 };
		final int target = 4;
		final int expectedOutput = 0;
		final int output = search(nums, target);
		Assert.assertEquals(expectedOutput, output);
	}

	@Test
	public void exampleOneB() {
		final int[] nums = { 4, 5, 6, 7, 0, 1, 2 };
		final int target = 5;
		final int expectedOutput = 1;
		final int output = search(nums, target);
		Assert.assertEquals(expectedOutput, output);
	}
	
	@Test
	public void exampleOneC() {
		final int[] nums = { 4, 5, 6, 7, 0, 1, 2, 3 };
		final int target = 5;
		final int expectedOutput = 1;
		final int output = search(nums, target);
		Assert.assertEquals(expectedOutput, output);
	}
	
	@Test
	public void exampleOneD() {
		final int[] nums = { 4, 5, 6, 7, 8, 9, 0, 1, 2, 3 };
		final int target = 4;
		final int expectedOutput = 0;
		final int output = search(nums, target);
		Assert.assertEquals(expectedOutput, output);
	}
	
	@Test
	public void exampleOneE() {
		final int[] nums = { 4, 5, 6, 7, 8, 9, 0, 1, 2, 3 };
		final int target = 3;
		final int expectedOutput = 9;
		final int output = search(nums, target);
		Assert.assertEquals(expectedOutput, output);
	}
	
	@Test
	public void exampleOneF() {
		final int[] nums = { 4, 5, 6, 7, 8, 9, 0, 1, 2, 3 };
		final int target = 2;
		final int expectedOutput = 8;
		final int output = search(nums, target);
		Assert.assertEquals(expectedOutput, output);
	}
	
	@Test
	public void exampleTwo() {
		final int[] nums = { 4, 5, 6, 7, 0, 1, 2 };
		final int target = 3;
		final int expectedOutput = -1;
		final int output = search(nums, target);
		Assert.assertEquals(expectedOutput, output);
	}

	@Test
	public void exampleThree() {
		final int[] nums = { 1 };
		final int target = 0;
		final int expectedOutput = -1;
		final int output = search(nums, target);
		Assert.assertEquals(expectedOutput, output);
	}

	@Test
	public void exampleFour() {
		final int[] nums = { 1, 3 };
		final int target = 3;
		final int expectedOutput = 1;
		final int output = search(nums, target);
		Assert.assertEquals(expectedOutput, output);
	}

	@Test
	public void exampleFive() {
		final int[] nums = { 3, 1 };
		final int target = 1;
		final int expectedOutput = 1;
		final int output = search(nums, target);
		Assert.assertEquals(expectedOutput, output);
	}
	
	@Test
	public void exampleSix() {
		final int[] nums = { 1, 3, 5 };
		final int target = 1;
		final int expectedOutput = 0;
		final int output = search(nums, target);
		Assert.assertEquals(expectedOutput, output);
	}
	
	@Test
	public void exampleSeven() {
		final int[] nums = { 4, 5, 6, 7, 8, 1, 2, 3 };
		final int target = 8;
		final int expectedOutput = 4;
		final int output = search(nums, target);
		Assert.assertEquals(expectedOutput, output);
	}
	
	@Test
	public void exampleSevenA() {
		final int[] nums = { 4, 5, 6, 7, 8, 1, 2, 3 };
		final int target = 7;
		final int expectedOutput = 3;
		final int output = search(nums, target);
		Assert.assertEquals(expectedOutput, output);
	}
	
	@Test
	public void exampleSevenB() {
		final int[] nums = { 4, 5, 6, 7, 8, 1, 2, 3 };
		final int target = 1;
		final int expectedOutput = 4;
		final int output = search(nums, target);
		Assert.assertEquals(expectedOutput, output);
	}
	
	public int search(int[] nums, int target) {
        int offset = -1;
        int left = 0;
        int right = nums.length-1;
        int check = -1;
        check = (right - left) / 2;
        if (nums[check] == target) {
            return check;
        } else if (target == nums[0]) {
        	return 0;
        } else if (target == nums[nums.length-1]) {
        	return nums.length-1;
        } else if (target > nums[check]) {
        	left = check;
        } else if (target < nums[nums.length-1] && target < nums[check]) {
        	left = check+1;
        	right = nums.length-1;
        } else if (target < nums[0]) {
        	left = check+1;
        	right = nums.length-1;
        } else {
	        right = check;
	    }
        if (left == right && nums[left] == target) {
        	return left;
        }
        
        while (left < right) {
            check = (right - left) / 2 + left;
            if (nums[check] == target) {
                return check;
            } else if (nums[left] > nums[check]) {
            	left = check + 1;
            	continue;
            } else if (nums[left] == target) { 
            	return left;
            } else if (nums[right] == target) {
        		return right;
        	} else if (target > nums[check]) {
            	if (left == check) {
            		left++;
            	} else {
            		left = check;
            	}
            } else {
            	if (right == check) {
            		right--;
            	} else {
            		right = check;
            	}
            }
        }
        return -1;
    }
}
