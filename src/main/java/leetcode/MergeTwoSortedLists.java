package leetcode;

import org.junit.Assert;
import org.junit.Test;

public class MergeTwoSortedLists {
	@Test
	public void exampleOne() {
		ListNode list1 = ListNode.toList(new int[] { 1, 2, 4});
		ListNode list2 = ListNode.toList(new int[] { 1, 3, 4});
		
		ListNode expected = ListNode.toList(new int[] { 1, 1, 2, 3, 4, 4 });
		ListNode output = this.mergeTwoLists(list1, list2);
		Assert.assertEquals(expected.toString(), output.toString());
	}
	
	@Test
	public void exampleTwo() {
		ListNode list1 = ListNode.toList(new int[] { });
		ListNode list2 = ListNode.toList(new int[] { });
		
		ListNode expected = ListNode.toList(new int[] {  });
		ListNode output = this.mergeTwoLists(list1, list2);
		Assert.assertEquals(expected, output);
	}
	
	@Test
	public void exampleThree() {
		ListNode list1 = ListNode.toList(new int[] { });
		ListNode list2 = ListNode.toList(new int[] { 0});
		
		ListNode expected = ListNode.toList(new int[] { 0 });
		ListNode output = this.mergeTwoLists(list1, list2);
		Assert.assertEquals(expected.toString(), output.toString());
	}
	
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
    	ListNode output = null;
    	ListNode currentTail = null;
        while (l1 != null || l2 != null) {
        	ListNode tail = null;
        	if (l1 != null && l2 != null) {
        		if (l1.val < l2.val) {
        			tail = new ListNode(l1.val);
        			l1 = l1.next;
        		} else {
        			tail = new ListNode(l2.val);
        			l2 = l2.next;
        		}
        	} else if (l2 == null) {
        		tail = new ListNode(l1.val);
        		l1 = l1.next;
        	} else {
        		tail = new ListNode(l2.val);
        		l2 = l2.next;
        	}
        	
        	if (output == null) {
        		output = tail;
        		currentTail = tail;
        	} else {
        		currentTail.next = tail;
        		currentTail = tail;
        	}
        }
        return output;
    }

}
