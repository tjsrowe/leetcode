package leetcode;

import java.util.LinkedList;

import org.junit.Assert;
import org.junit.Test;


public class TreeNode {
	public static TreeNode parseString(final String data) {
		String shortened = data.trim().replace(" ", "");
		String substring = shortened.substring(1, shortened.length() - 1);
		if (substring.length() == 0) {
			return null;
		}
		String[] parts = substring.split(",");
		// TreeNode[] nodes = new TreeNode[parts.length];
		for (int i = 0; i < parts.length; i++) {
			if ("null".equalsIgnoreCase(parts[i])) {
				parts[i] = null;
			}
		}
		TreeNode tree = buildTreeFromArray(parts);
		// buildTreeFromArray(0, parts, nodes);
		return tree;
	}

	static TreeNode buildTreeFromArray(String[] parts) {
		LinkedList<TreeNode> stack = new LinkedList<TreeNode>();
		if (parts[0] == null) {
			return null;
		}
		int val = Integer.parseInt(parts[0]);
		TreeNode node = new TreeNode(val);
		TreeNode top = node;
		stack.addFirst(node);
		node = new TreeNode(val);

		for (int i = 1; i < parts.length; i++) {
			TreeNode parent = stack.pollFirst();
			if (parts[i] != null) {
				int nodeVal = Integer.parseInt(parts[i]);
				node = new TreeNode(nodeVal);
				parent.left = node;
				stack.addLast(node);
			}
			i++;
			if (i < parts.length) {
				if (parts[i] != null) {
					int nodeVal = Integer.parseInt(parts[i]);
					node = new TreeNode(nodeVal);
					parent.right = node;
					stack.addLast(node);
				}
			}
		}
		return top;
	}

	static TreeNode buildTreeFromHeap(int index, String[] parts, TreeNode[] nodes) {
		if (parts[index] != null) {
			int leftNodeIndex = (index * 2) + 1;
			int rightNodeIndex = (index * 2) + 2;
			TreeNode leftNode = null;
			if (parts.length > leftNodeIndex) {
				leftNode = buildTreeFromHeap(leftNodeIndex, parts, nodes);
			}
			TreeNode rightNode = null;
			if (parts.length > rightNodeIndex) {
				rightNode = buildTreeFromHeap(rightNodeIndex, parts, nodes);
			}
			try {
				int val = Integer.parseInt(parts[index]);
				TreeNode node = new TreeNode(val, leftNode, rightNode);
				nodes[index] = node;
				return node;
			} catch (NumberFormatException ex) {
				return null;
			}
		} else {
			return null;
		}
	}

	public int val;
	public TreeNode left;
	public TreeNode right;

	TreeNode() {
	}

	TreeNode(int val) {
		this.val = val;
	}

	TreeNode(int val, TreeNode left, TreeNode right) {
		this.val = val;
		this.left = left;
		this.right = right;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.val);
		
		if (this.left != null || this.right != null) {
			String leftVal = this.left != null ? left.toString() : "null";
			String rightVal = this.right != null ? right.toString() : "null";
			sb.append(",")
			.append(leftVal)
			.append(",")
			.append(rightVal);
		}
		return sb.toString();
	}
}
