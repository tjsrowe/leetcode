package leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class BinaryTreePreorderTraversal {
	@Test
	public void exampleOne() {
		TreeNode data = TreeNode.parseString("[1,null,2,3]");
		List<Integer> output = this.preorderTraversal(data);

		Assert.assertEquals(Arrays.asList(1, 2, 3), output);
	}

	@Test
	public void exampleTwo() {
		TreeNode data = TreeNode.parseString("[]");
		List<Integer> output = this.preorderTraversal(data);

		Assert.assertEquals(Arrays.asList(), output);
	}

	@Test
	public void exampleThree() {
		TreeNode data = TreeNode.parseString("[1]");
		List<Integer> output = this.preorderTraversal(data);

		Assert.assertEquals(Arrays.asList(1), output);
	}

	@Test
	public void exampleFour() {
		TreeNode data = TreeNode.parseString("[1,2]");
		List<Integer> output = this.preorderTraversal(data);

		Assert.assertEquals(Arrays.asList(1, 2), output);
	}

	@Test
	public void exampleFive() {
		TreeNode data = TreeNode.parseString("[1,null,2]");
		List<Integer> output = this.preorderTraversal(data);

		Assert.assertEquals(Arrays.asList(1, 2), output);
	}

	public List<Integer> preorderTraversal(TreeNode root) {
		List<Integer> output = new ArrayList<Integer>();

		if (root != null) {
			output.add(root.val);
			if (root.left != null) {
				List<Integer> left = preorderTraversal(root.left);
				output.addAll(left);
			}
			if (root.right != null) {
				List<Integer> right = preorderTraversal(root.right);
				output.addAll(right);
			}
		}

		return output;
	}

}
