package leetcode;

import org.junit.Assert;
import org.junit.Test;

public class LongestCommonPrefix {
	@Test
	public void exampleOne() {
		final String [] input = new String[] { "flower", "flow", "flight" };
		final String output = "fl";
		final String result = this.longestCommonPrefix(input);
		Assert.assertEquals(output,  result);
	}

	@Test
	public void exampleTwo() {
		final String [] input = new String[] { "dog", "racecar", "car" };
		final String output = "";
		final String result = this.longestCommonPrefix(input);
		Assert.assertEquals(output,  result);
	}
	
	@Test
	public void exampleThree() {
		final String [] input = new String[] { "a" };
		final String output = "a";
		final String result = this.longestCommonPrefix(input);
		Assert.assertEquals(output,  result);
	}
	
	@Test
	public void exampleFour() {
		final String [] input = new String[] { "ab", "a" };
		final String output = "a";
		final String result = this.longestCommonPrefix(input);
		Assert.assertEquals(output,  result);
	}
	
	@Test
	public void exampleFive() {
		final String [] input = new String[] { "a", "ab" };
		final String output = "a";
		final String result = this.longestCommonPrefix(input);
		Assert.assertEquals(output,  result);
	}
	
    public String longestCommonPrefix(String[] strs) {
        for (int chr = 0;chr < strs[0].length();chr++) {
        	char c = strs[0].charAt(chr);
        	for (int str = 1;str < strs.length;str++) {
        		if (chr >= strs[str].length()) {
        			return strs[0].substring(0, chr);
        		}
        		if (c != strs[str].charAt(chr)) {
        			if (chr == 0) {
        				return "";
        			} else {
        				return strs[0].substring(0, chr);
        			}
        		}
        	}
        }
        return strs[0];
    }
}
